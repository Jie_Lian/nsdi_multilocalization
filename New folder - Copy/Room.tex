\section{System design}
In this section, we present our design of the system.
We let the speaker generate a continuous wave, then let the microphone capture the Doppler signal from the targets.
%Our system would utilize the  the phase of the Doppler signal for localization.
Fine-grained analysis is employed on the reflections to extract the phase and frequency of the Doppler signal for localization.
To accurately localize the targets, a set of techniques should be developed.
We will face a set of new challenges.
\begin{itemize}
	
	\item The received signal is a combination of a set of noises, which include the reflection from surrounding objects and system defects.	
	The reflections from the surrounding object could cause the unwanted phase change of the signal.
	The system defects would decrease the SNR of the signal.
	Thus such two types of noises must be eliminated.
	
	
	\item A person could be seen as a cluster of different moving body segments (e.g., head, torso, arms, and legs).
	Such segments would have different speeds while moving, thus generating different Doppler signals.
	We need to develop a method to isolate the signal for each body segment and then fuse the reflection from different body segments together to track the target's movement accurately.
	
	
	\item  For phase estimation, I-Q decomposition is employed.
	In  I-Q decomposition, we need to estimate the signal's phase by multiplying the original signal with a baseband signal that has the same frequency as the original signal. 
	However, due to the relatively high speed of walking, the Doppler effect would introduce frequency changes in the received signal.
	For example, consider a continuous acoustic wave with a  frequency of $f_{c}=20 \mathrm{kHz}$ reflected by a walking person with a speed of $v=1m/s$, and assume the receiver is static.
	Then according to the equation \ref{doppler_formula}, the movement would cause about $58Hz$ Doppler shift.
	Thus the frequency difference between the original signal and the baseband signal would introduce considerable error when estimating the phase.
	We must develop a solution to accurately estimate the frequency of the Doppler signal to accurately estimate the phase.
	
	
	
	\item The multipath effect would introduce errors when estimating the target position.
	Since the multipath is changing with the target moving, it is hard to eliminate the multipath effect directly.
	Sometimes, the multipath would be recognized as the reflection from a  nonexistent target.
	Thus, we must develop solutions to differentiate the multipath and target signals.
	
	
\end{itemize}


\subsection{Sensing}
We control the speaker to transmit the continuous wave at frequency $f_{0}=20kHz$, which could be represented as:
%We will let a speaker transmit the FMCW signal. 
%A FMCW signal of duration $T$ with frequency ranging from $f_{0}$ to $(f_{0}+B)$ could be represented as
\begin{equation}
	F(t)=A \cos \left(2 \pi f_{0} t\right).
\end{equation}
The Doppler frequency would be generated when the signal is reflected by the moving target.
Then a microphone array is used to collect the signal at a sampling frequency of 44.1kHz.
44.1khz is twice higher than the highest Doppler frequency, ensuring the signal could be completely reconstructed.

We will let the speaker keep sending the continuous wave and let the system senses the sound pressure level between 19kHz and 21kHz.
Once the power is higher than a certain level, our system will start processing the signal for localization.





\subsection{Signal Processing}
This section will process the signal to eliminate the interference and split the signal into a set of narrowband signals.


\subsubsection{Short Time Fourier Transform}

We will generate the spectrum of the signal via short-time Fourier transform (STFT).
To be specific, the original signals will be sliced by a set of small windows with a length of 0.3s. 
The overlap between each two consecutive windows is  95\%, which means 1s signal would be sliced into 66 bins.
We will multiply each small bin by a Hamming window and apply the 21000 points Fast Fourier transform (FFT) on each frame.
We divide the frequency into 21000 sub-bands, this will give a frequency resolution of about 1hz.
After the above process, the spectrum would be generated for further analysis.

\subsubsection{Interference Cancellation}
\begin{figure}
	\centering
	\includegraphics[width=3.3in]{fig/denoised.jpg}
	\caption{Denoised spectrogram.}
	\label{Denoised}
\end{figure}
The received signal would contain the target signal and reflections from other objects.
Usually, a set of static objects (e.g., walls, furniture, and ceiling) exist in a room. 
These static objects can be seen as a set of static reflectors.
A single moving person could be considered as a cluster of different moving body segments (e.g., head, torso, arms, and legs).
As such, when reflecting from the targets,  the signal can be seen a combination of the original signal and a set of the delayed version of the transmitted signal.
Then the reflection could be expressed as:
%When tracking one target, a target (moving body) would reflect the signal. 
%As figure \ref{txrx} shows, the  reflected signal could be seen as the delayed version of the transmitted signal. 
%The reflected signal  can be expressed as:
\begin{equation}
	\begin{aligned}
		R(t)=A \cos \left(2 \pi f_{0} t\right) + \sum \alpha_{m} \cos \left(2 \pi f_{m} (t-\tau_{m})\right)  \\
		+ \sum A_{n} \cos \left(2 \pi f_{0} (t-\tau_{n})\right)+N(t), \label{mixx}
	\end{aligned}
\end{equation}
where $A \cos \left(2 \pi f_{0} t\right)$ is the direct transmitted original signal.
The $\alpha_{m} \cos \left(2 \pi f_{m} (t-\tau_{m})\right)$ represent  the combination of the signal reflected from moving persons (e.g., different body parts and different person).
Since the human moving would generate the Doppler effect, the Doppler frequency $f_{m}$ is different from the $f_{0}$.   
The $A_{n} \cos \left(2 \pi f_{c} (t-\tau_{n})\right)$ represent the signal reflected from the surrounding object.
The objects are static, so reflections have the same frequency with the $f_{0}$.  
And also, the noise $N(t)$ that caused by system defects have been recorded by the microphone.
%From the equation, we could see that the received signal contain the reflection of different body part (i.e. the frequency and amplitude of the reflection from different body part denote as $\alpha_{1}, \alpha_{2},\alpha_{3}$ and $f_{1},f_{2},f_{3}$, respectively)
%And also the noise $N(t)$ that caused by system defects has been recorded by the microphone.$
To accurately track the target movement, direct transmission signal and signal reflected from surrounding object,   and the noise caused by system defects must be eliminated.


We perform a two-stage interference cancellation scheme   on the spectrum to remove the interference from static objects and system defects.

\textbf{Stage 1:} The direct transmission signal and the signal reflected from surrounding objects have the same frequency  $f_{0}=20kHz$. 
We could simply set the frequency at $20kHz$ to 0 to eliminate the interference of the direct transmission and signal reflected from the surrounding object.
However, the typical home speaker is not designed to generate high-frequency ultrasound.
The frequency it generated would fluctuate slightly over time, and thus could not be completely eliminated by directly setting the frequency at 20khz to 0.
We have observed that the ultrasound signal fluctuate range is from 19.99khz to 20.01khz.
So we could set the spectrum value between 19.99khz to 20.01khz to fully remove the direct transmitted noise and reflection from surrounding objects.

\textbf{Stage 2:} Typically the noise in the ultrasound domain is caused by system defects.
Such type of noise could be seen as the points spreading on the spectrogram.
This noise does not vary with time, so we could directly subtract it by spectral subtraction.
That is, we record the noise in the static environment, then subtract the spectrum of the noise from the current signal.
Then the remaining is the spectrum without system defects.

Figure \ref{Denoised} shows the denoised spectrum after applying the noise cancellation.
We could see the Doppler shift caused by the target's movement clearly stand out.
The Doppler shift varies over time, which represents the different instantaneous speeds caused by different movements or different targets.


\subsubsection{Slicing the Signal}
After the above process, we get a relatively clean spectrum.
The remain signal could be represented as
\begin{equation}
	\begin{aligned}
		R(t)=\sum \alpha_{m} \cos \left(2 \pi f_{m} (t-\tau_{m})\right), \label{1114}
	\end{aligned}
\end{equation}
the above equation shows that the signal is the superposition of multiple sinusoid waves with different Doppler shifts.
And our goal is to isolate the Doppler shift corresponding to each body segment(leg, torso, arm).


To isolate the signal, we will analyze the Short-Time Fourier Transformation of the signal.
We could get a coarse estimation of $f_{m}$ by identifying the peak of the signal’s Fourier Transformation.
We calculate the average difference between each $f_{m}$, and the average difference is about 20hz.
%The valleys on the signal restricting the bandwidth.
Thus we will divide the signal based on the frequency difference between the $f_{m}$. 
In this way, we will divide the entire spectrogram into a set of $20hz$ narrowband signals.
%In our experiment, we will set the bandwidth of the narrow band signal as $20hz$.
%The bandwidth is about $20hz$.
The $20hz$ could give a $5cm/s$ velocity resolution, enough to capture the fine-grained velocity difference of different body parts.
Even though two people have the same moving speed, some of their body segments would have different velocities and then be recorded in the different narrowband signals.

Since the 20hz signal is narrow enough, then the signal could be approximately seen as a single frequency signal. 
Thus the narrowband signal could be seen as the reflection from a single reflector.
Figure \ref{Denoised} shows we only need to consider the signal from 19500hz to 20500hz.
This range is sufficient to cover all the Doppler shifts from a moving person. 
%Each narrow band signal considered only contain Doppler frequency from a single reflectors.
Thus, each narrowband signal  could be represented as: 
\begin{equation}
	\begin{aligned}
		R(t)=\alpha_{m} \cos \left(2 \pi f_{m} (t-\tau_{m})\right), \label{1114}
	\end{aligned}
\end{equation}
where $f_{m}$ is the corresponding frequency of the target in sub band,   $\tau_{m}$ is the corresponding TOF.
%However not all narrowband signal contain the Doppler reflection, thus the narrowband signal that the average amplitude is lower than a threshold would be discarded.
%Then we will performing Inverse Fast Fourier transform (IFFT) on the spectrogram to reconstruct the  narrow band signal for further analysis.





\subsection{Signal Model}
This section will develop a signal model to formulate the narrow band signal with the representation of four parameters, {\em i.e.,} range, angle, TOF, and velocity. 

%In this section, we introduce the signal model of our system.
%We could model the signal with the target's range, angle, and velocity through the signal model.
%In this section, we introduce the signal model of our system.
%To show how to eliminate the interference of the signal, and extract the phase of the signal so that we could analyze the phase of the signal to get the velocity and distance changing in room scale environment.



%allows us to fully characterize the targets’ location and motion status.


\begin{comment}
content...

\textbf{Stage 2:} We further mitigate the other irreverent interference by compute the motion  curve.
use frequency diversity to mitigate the interference of other body part.
Each body part would have different motion speed in the moving process, thus generating different Doppler frequency which  could be denoted as  $f_{1},  f_{2},  f_{3}$.
Among them the torso has the lowest speed thus generating the lowest Doppler frequency.
Also the torso has the largest reflection size, so has the most strong Doppler reflection.
By this fact we could set a threshold to eliminate the interference of other body part.
That is we calculate the torso contour curve and set the value above the threshold to 0.
The remain signal is mostly caused by the torso.

After above process, we get a relatively clean spectrogram.
Then applying the IFFT on the spectrogram we could get the clean signal that reflected from torso, expressed us
\begin{equation}
R(t)=\alpha_{1} \cos \left(2 \pi f_{1} (t-\tau_{1})\right)
\end{equation}
However directly measure the $\tau_{1}$ in a continuous wave is impossible.
However we could measure the phase of the signal to get the changing of the $\tau_{1}$.
\end{comment}

\begin{figure}
	\centering
	\includegraphics[width=3.3in]{fig/IQ.pdf}
	\caption{I-Q decomposition.}
	\label{IQ}
\end{figure}

\subsubsection{Signal Model}
After above process, we get the narrowband signal that corresponding to a single target, which could be represented as $\cos \left(2 \pi f_{m}\left(t-\tau_{m}\right)\right)$.
In the equation, the $f_{m}$ is related to the velocity of the target.
And the phase of the signal is determined by the TOF, which is affected by the distance, velocity, and AOA of the target.
In each round, we will take a 0.1 s narrowband signal for estimation.
Not all narrowband signals contain the Doppler reflection. Thus the narrowband signal that the average amplitude is lower than a predefined threshold would be discarded.
Then, by analyzing the phase and frequency of the remaining signals, we could get the motion status  {\em i.e.,} range, angle, and velocity of the target.




We will apply the I-Q decomposition on  each narrowband signal.
As shown in Figure~\ref{IQ}, in I-Q decomposition, the signal $R(t)$ is multiplied with the continuous wave $f_{c}$, where the  $f_{c}$ should have the same frequency as the signal $R(t)$, and its 90-degree phase-shifted version.
%the carrier signal of the original signal
According to the fact that  $\cos A  \cos B=\frac{1}{2}(\cos (A+B)+\cos(A-B))$, we can get  $\cos(A-B)$ by  filtering the high frequency component $\cos (A+B)$ through a low pass filter.
Thus the In-Phase component can be expressed as follows:
\begin{equation}
	I(t) =\frac{1}{2} \alpha_{1} \cos 2 \pi \left((f_{m}-f_{c})t + f_{m} \tau\right)  .  	\label{combin1eI}
\end{equation}
Similarly, the Quadrature   signal can also be represented as:
\begin{equation}
	I(t) =\frac{1}{2} \alpha_{1} \sin 2 \pi \left((f_{m}-f_{c})t + f_{m} \tau\right)  . 
\end{equation}
Combing In-Phase signal and Quadrature  signal, we get a complex mixed signal, yielding
\begin{equation}
	S^{M}(t)=I(t)+j Q(t)=\frac{1}{2} \alpha_{1} e^{j 2 \pi \left((f_{m}-f_{c})t + f_{m} \tau\right)  }
	\label{combin1e}.
\end{equation}

Then we will analyze the frequency and phase of the complex signal for range, angle, and velocity estimation.

\noindent\textbf{Frequency Analyze:} According to equation \ref{combin1e}, the frequency of the complex signal is determined by the difference between $f_{m}$ and $f_{c}$.
Where $f_{m}$ is the Doppler frequency of the narrowband signal, and $f_{c}$ is the estimated Doppler frequency  of the narrowband signal.
When we find the  $f_{c}$ that most close to the $f_{m}$, the frequency of the $S^{M}(t)$ is minimized, and the variance of the $S^{M}(t)$ would be minimized.
Thus to find the correct $f_{c}$, we will shift the frequency of $f_{c}$ and apply the I-Q decomposition with different $f_{c}$. 
We will compute the variance of $S^{M}(t)$ , until the variance is minimized.
That means we found the $f_{c}$ that most close to $f_{m}$.
Since the $f_{c} \approx f_{m}=\frac{c+v}{c-v} f_{0}$, we could get the velocity parameter of the narrowband signal through the $f_{c}$.
Then the complex signal could be represented as  
\begin{equation}
	S^{M}(t)=I(t)+j Q(t)=\frac{1}{2} \alpha_{1} e^{j 2 \pi \left(f_{small}t + f_{c} \tau\right)  }
	\label{combine11}
\end{equation}
The $f_{small}$ is a small value.
Ideally, the  $f_{small}$ should be 0. However, due to the small difference between $f_{c}$ and $f_{m}$, a low-frequency component would be created.


The complex signal may still contain noise due to the influence of multipath.
In a complex plane, we could denote the signal received at  microphone as a set of combination of static vector and dynamic vector.
The static vector caused by multipath would affect the phase estimation accuracy.
To estimate the static vector, we apply the Local Extreme Value Detection (LEVD) algorithm.
Then the Local Extreme Value Detection algorithm initializes the static vector estimation by averaging the recordings over the past 1 second.
The real part is initialized by calculating the average value of the (I) and then the imaginary part is calculated by averaging the (Q).
The static vector would change when the target is moving.
For example, when a human is moving, the path from the speaker to the body, then to the nearby wall, and finally to the microphone would change.
When the local extrema value of the I and Q exceed a predefined threshold, we will update the static vector by setting its real part and imaginary part as the average of the last pair of local maximum and minimum values.
In this way, we could update the static vector dynamically.
Then subtracting the static vector, we could get the pure dynamic vector of the complex signal.
%After getting the above  reflected signal, we will apply the I-Q decomposition to get the phase of the signal.
% of the carrier signal, .
%This process is to get the  In-Phase (I) and Quadrature  (Q) parts of the reflected signal.
%Take In-Phase signal as an example.Th
\begin{figure}
	\centering
	\includegraphics[width=3.3in]{fig/Array.pdf}
	\caption{Additional distance caused by the microphone array structure.}
	\label{array}
\end{figure}

\noindent\textbf{Phase Analyze:} We further analyze the phase of the pure dynamic vector of the complex signal.
The phase is determined by $\tau$, so we analyze the $\tau$ first.
Suppose that a target is moving at a  velocity of $v$ at the distance of $r$ and the angle of $\theta$.
Assume we receive the reflected signal from the target by a liner 4-microphone array, the distance between each microphone is $d$,
and receives the reflected signal from the target after $\tau$ s of the signal transmitted from the speaker. 
So in such a small period of time, the movement will cause an additional round-trip-time of $\frac{2vt_{d}}{c}$.
On the other hand, as figure \ref{array} shows, for the $k^{\text {th }}$ microphone, the respective TOF of received signal will propagate an extra time of $\frac{(k-1)d \cos \theta}{c}$ compared to the first microphone, which is the rightmost microphone in figure \ref{array}, on the microphone array.
The $\theta$  represents the AoA of the signal. 
%On the other hand, as shown in Figure~\ref{array}, for the $k^{\text {th }}$ microphone, the respective TOF of received signal will propagate an extra time of $\frac{(k-1) (k-1)d \cos \theta}{c}$ compared to the first microphone, where $d$ and $\theta$ represents the distance between two adjacent microphones and the AoA of signal at this microphone, respectively. 
Hence, TOF for signal received at the $k^{\text {th}}$ microphone  can be computed as
\begin{equation}
	\tau=\frac{2 r}{c}+\frac{2v\tau}{c}+\frac{(k-1)d \cos \theta}{c}. \label{tof}
\end{equation}
Thus, applying the above equation to equation \ref{combin1eI}, we have 
\begin{equation}
	\begin{aligned}
		I(t) & =\frac{1}{2} \alpha_{1} \cos 2 \pi \left(f_{small}t + f_{c} \tau\right) \\
		& = \frac{1}{2} \alpha_{1} \cos 2 \pi \left(f_{small}t + f_{c} (\frac{2 r}{c}+\frac{2v\tau}{c}+\frac{(k-1)d \cos \theta}{c}) \right) \\
		%& = \frac{1}{2} \alpha_{1} \cos 2 \pi \left( \frac{3v}{c}f_{c}t + f_{c} \frac{2R_{inital}}{c}  - f_{c} \frac{(k-1)d \cos \theta}{c}\right)\\
		%&+ \frac{1}{2} \alpha_{1} \cos 2 \pi \left( f_{c} \frac{2R_{inital}v}{c^2}+f_{c} \frac{2v^2}{c^2} - f_{c} \frac{v(k-1)d \cos \theta}{c^2} \right) \\
		%& \approx \frac{1}{2} \alpha_{1} \cos 2 \pi \left( \frac{3v}{c}f_{c}t + f_{c} \frac{2R_{inital}}{c}  +f_{c} \frac{2R_{inital}v}{c^2}- f_{c} \frac{(k-1)d \cos \theta}{c}\right)
	\end{aligned}
\end{equation}
%Notably, $2\frac{v^2}{c^2}f_{c}t$ is much smaller than $\frac{3v}{c}f_{c}t$ , so $2\frac{v^2}{c^2}f_{c}t$can be ignored.
%Similarly, the $f_{c} \frac{v(k-1)d \cos \theta}{c^2}$ is also small, that can be ignored
Similarly, the Quadrature   signal can also be represented as
\begin{equation}
	Q(t)= \frac{1}{2} \alpha_{1} \sin 2 \pi \left(f_{small}t + f_{c}(\frac{2 r}{c}+\frac{2v\tau}{c}+\frac{(k-1)d \cos \theta}{c}) \right)
\end{equation}
Then finally we have  
\begin{equation}
	S^{M}(t)=I(t)+j Q(t)=\frac{1}{2} \alpha_{1} e^{j 2 \pi \left(f_{small}t + f_{c}(\frac{2 r}{c}+\frac{2v\tau}{c}+\frac{(k-1)d \cos \theta}{c}) \right)  },
	\label{combine}
\end{equation}
where the $f_{c}$ is the Doppler frequency which has been estimated.
We could simplify the above complex mixed signal as $S^{M}= \frac{1}{2} \alpha \cdot F \cdot R \cdot T \cdot \Theta $.
We could represent the 4 component as:
\begin{equation}
	\begin{aligned}
		&F=e^{j 2 \pi  f_{small}t} \\
		&R_{v}=e^{j 2 \pi \frac{2r}{c}f_{c}}  \\
		&T_{v}=e^{j 2 \pi \frac{2v\tau}{c}f_{c}}\\
		&\Theta_{v}=e^{j 2 \pi \frac{(k-1)d \cos \theta}{c}f_{c}} \\
	\end{aligned}
\end{equation}
Note that the $f_c$ has already been estimated, and we could directly compute the $v$ through the estimated$fc$. 
Thus we only need to estimate three parameters $r$, $\tau$, $\theta$ for each narrow band signal.

In one target scenario, each narrowband signal contains the reflections of body segments from a single target.
So, although the velocity of the body segments may be different, the  $r$, $\tau$, $\theta$ would have similar values.
We could easily find the target location by cluster algorithm.
However, in the multi-target scenario, the narrowband  Doppler signal would be caused by body segments from the different targets.
We shall develop a new solution in Section~\ref{Multi Target Estimator} to estimate the location of the target and calculate the parameters corresponding to each target.


\begin{comment}
content...


Ordinary We  apply the I-Q decomposition to get the phase of the signal.
Usually in I-Q decomposition, we need to multiply the signal with a carrier frequency that  have the same frequency of the signal.
However, in our condition due to the Doppler effect, there is considerable difference between the frequency of the received signal and carrier frequency. 
Such difference would generate unwanted frequency commonet, distort the frequency component.

So we need to find a frequency $f_{r}^{'}$ that similar to the $f_{r}$ mitigate the error caused by freqeuncy shift. 

Thus the modified I-Q decomposition is that, we generate a continuous wave that have the same phase with the original signal. 

That is, we set its upper bound   to 20kHz, to completely filter out the high-frequency component. 
Mathematically, the In-Phase signal can be expressed as follows: 
%The upper bound of the band-pass filter is set to 25kHz, and the lower bound is set to 16kHz to completely filter out the high-frequency component and the low-frequency noise.
%After the process of the band-pass filter, we got the In-Phase signal which could be represented as 
\begin{equation}
I(t) =\frac{1}{2} \alpha_{1} \cos 2 \pi \left((f_{r}^{'}-f_{r})t + f_{r} \tau\right)  , \label{mix_1}
\end{equation}
Where the $f_{t}$ is the Doppler frequency of the target, $\tau$ is the TOF of the target.
Since the $f_{r}^{'}$ is similar to $f_{r}$, the $(f_{r}^{'}-f_{r})$ is always small.
The $f_{r} \tau$ is related to the initial phase of the signal






Combing In-Phase signal and Quadrature  signal, we get a complex mixed signal, yielding
\begin{equation}
S^{M}(t)=I(t)+j Q(t)=\frac{1}{2} \alpha_{1} e^{j 2 \pi \left((f_{r}^{'}-f_{r})t + \frac{c+v}{c-v}f_{c} (\frac{2 r}{c}+\frac{2v\tau}{c}+\frac{(k-1)d \cos \theta}{c}) \right) }
\label{temp2}
\end{equation}

\end{comment}


%The round-trip distance of the signal determines the TOF $\tau$.
%The round-trip distance  is affected by the motion of the targets, which is determined by the range, velocity, and angle.
%We could analyze the phase of the complex mixed-signal to get the target's range, velocity, and angle.


%We apply the MEVS to dynamicly estimate the static component of the signal to remove the intereference of the static component.
%We estimate the phase change in 0.1s to get the motion status of one target.
%The combing the phase change in a period of time, we could get the distance change of the target.

\begin{figure*}%[!htb]
	\centering
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{fig/multipath1.pdf}
		\caption{Cluster points at time t.}
		\label{multipath1}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.30\textwidth}
		\centering
		\includegraphics[width=2.0in]{fig/multipath2.pdf}
		\caption{Cluster points at time t+1.}
		\label{multipath2}
	\end{minipage}
	
	
\end{figure*}

\subsection{Target Estimator}
\label{Multi Target Estimator}
In this section, we will estimate the  $r$, $\tau$, $\theta$ of the narrowband signals and cluster the estimated parameters for the corresponding targets.

\subsubsection{Motion State Estimator}
In each round of estimation, we use the signal that lasts for $0.1s$.
Given the sampling rate of $44.1khz$, the signal contains $N=4410$ samples.
At each sample, we have signal samples from $K=4$ microphones.
At each microphone, we have $Z$ sub-bands.
Then, we could represent the signal samples as $\Sigma$, where $\Sigma=\left[S_{m1}, S_{m2} , \cdots , S_{m6}\right]^T$.
Each $S_{m}$ represents the complex signal received at each microphone that after the I-Q decomposition, has a size of $Z \times 4410$.
Since the $S_{m}$ could be represented as a combination of a set of complex vectors or scalar, then we could build an optimization function by multiplying the conjugates vectors of $S_{m}$:
\begin{equation}
	\mathbf{E}(\theta, \tau, r)= \Sigma \cdot R^{*}(r) \cdot  T^{*}(\tau)  \cdot \Theta^{*}(\theta) ,
\end{equation}
where $(\cdot)^{*}$ is the conjugate operation. 
The $\Sigma$ has a size $(K\times Z) \times N$.
$R^{*}(r), T^{*}(\tau)$, and $\Theta^{*}$ are a $(K\times Z) \times 1$ vector.
When the correct $\theta, v, r$ are estimated, the function would achieve local maximum value.

Also according to Equation \ref{tof}, we have another optimization function:
\begin{equation}
	\mathbf{E}_{\tau}(\theta, \tau, r)= |\tau-(\frac{2 r}{c}+\frac{2 v \tau}{c}+\frac{(k-1)d \cos \theta}{c})|.
\end{equation}

Thus we could define the optimization function as
\begin{equation}
	\left(\theta, \tau, r \right)=\arg \max \left|\mathbf{E}(\theta, \tau, r) - \mathbf{E}_{\tau}(\theta, \tau, r) \right|.
\end{equation} 
After the above process, for a 4 microphone array, we will get the estimation from the N*4 bins, which contain the reflection from the targets and multi path. Thus we need to recognize which reflection is from the target and which is from the multi-path.

\begin{comment}
content...

\subsubsection{Compute the AOA Searching Range}
We estimate the AOA of the target by the beamforming.
To achieve a stronger signal reception at a particular direction, we can sum up the delayed version of signals
from multiple antennas.



The signals receive at the microphones are the delayed version of the signals arrive at the center of the microphone array.
When the signal arrives at $\theta$, the phase difference between the  signals arriving at $k$th microphone and the center of the microphone array is given by $\frac{d \cos \left(\theta-\phi_{k}\right)}{\lambda}$.
Thus an arbitrary signal arrival angle  $\theta$, a steering vector is thus defined to represent the phase difference between each antennas   as:
\begin{equation}
\omega(\theta)=\left[e^{j 2 \pi \frac{d \cos \left(\theta-\phi_{1}\right)}{\lambda}}, e^{j 2 \pi \frac{d \cos \left(\theta-\phi_{2}\right)}{\lambda}}, \ldots, e^{j 2 \pi \frac{d \cos \left(\theta-\phi_{k}\right)}{\lambda}}\right]
\end{equation}
We could compute the weighted sum of the signal across 6 microphones to build the beam formed signal at direction $\theta$, which could be expressed as:
\begin{equation}
y(\theta, t)=\sum_{i=1}^{M} \omega_{i}(\theta) \cdot R_{i}(t)
\end{equation}
Where the $R_{i}(t)$ is the complex signal that after the I-Q decomposition.
Then we will adjust the $\theta$ to find a set of local maximum value.
The result of the signal could be drawn as a figure.
The peak value that higher than 0.3*maxmium value would be selected as the candidate.
Then we will search for $\theta$ that higher than



\end{comment}




\subsubsection{Clustering the  Estimation}
Before identifying the multipath, we apply the $k$-means clustering to fuse the estimation together.
The signal from one person would have a relatively close $\theta, \tau, r, v$.
Even if the signals are from different body segments, although $v$ is different, the $\theta, \tau, r$ would be similar.
Thus the signal from one person could be clustered together.
The input of the $k$-means clustering is a set of points where the points could be denoted as.  ${o}_{i}=( \tau_{i}, r_{i}, v_{i}, \theta_{i} )$ 
Each point denotes a reflection point in 4-D space at location $\left(\tau_{i}, r_{i}, v_{i},\theta_{i}\right)$. 
The $K$ is the number of the clusters, which denote the total number of targets and multipath. 
Note that the $K$ is unknown, so the goal is to identify the number of clusters $K$, and get the corresponding locations of the cluster.
Directly applying the  $k$-means clustering algorithm does not get reliable results.
Therefore, a set of techniques is applied to enhance the $k$-means clustering algorithm.




The silhouette value \cite{thinsungnoena2015clustering} is involved to improve the $k$-means clustering.
We use silhouette value to evaluate the performance of $k$-means clustering.
The silhouette ranges from -1 to +1, where a high value indicates that the object is well matched to its own cluster and poorly matched to neighboring clusters.
Formally, the silhouette value of the $i$ th point is defined as
$$
s_{i}=\frac{b_{i}-a_{i}}{\max \left\{a_{i}, b_{i}\right\}}.
$$
Where $a_{i}$ is the average distance from the $i$ th point to the other points in the same cluster, and $b_{i}$ is the minimum average distance from the point to points in a different cluster. 
If most points have a high silhouette value, then the clustering configuration is good. 
If many points have a low or negative value, then the clustering configuration is not appropriate and may have too many or too few clusters.
Then the average of $s_{i}$ over all data points is used to evaluate how appropriately the data have been clustered. 
We will try $k$ in a proper range and select the value that produces the highest average silhouette value.


Now we have obtained the cluster number $k$.
However, there still exist errors.
There are two types of typical errors. 
First, the points of the two targets are falsely merged as one cluster.
Second, the points of one single target are wrongly divided into multiple clusters. 
We mitigate such errors by leveraging the geometric properties of the target (human body).

Two metrics are introduced to mitigate the errors.
%We first compute the centroid of each cluster by $k$-means algorithm. 
%Then we calculate two distances in the 2D plane. 
First is the intracluster method distance, which is the distance among points of a cluster. 
This metric gives a sense of how well the $k$-means was able to bring the estimation together.
The second is inter-cluster method distance, which shows the distance between the data point and the cluster center.
To correct the first type of error, we examine the number of points whose intracluster distance exceeds a pre-defined maximum value which represents the maximum of typical human size. 
If the selected points form a considerable portion of the cluster, they are then sorted out as a new cluster. 
In the second case, usually, the clusters from the same target are extremely close to each other. 
So we examine the inter-cluster method distance and merge those close clusters as one.


Now we have a set of clusters.
The centroid of each cluster would be seen as the location of the target reflection.


\subsubsection{Trace Estimation} 

After the above process, we will get the angel, range, velocity and TOF by the centroid of each clusters.
We could denote these as a set of  ``points'' in a 4-D space.
However, since the multipath signal and reverberation signal would be detected as the reflections from the targets.
Sometimes, such signal would have many clusters.
So, the ``points''   may not accurately correspond to the target refection, making it difficult to accurately identify the target position. 

We derive a method to identify the point that corresponds to the targets.
The target movement is continuous, and thus, the parameters between two consecutive estimates from the same target should be similar. 
The basic idea is that within a short period of time (0.1s), the  angel, range, velocity, and TOF changes of a target are small. 
As figure \ref{multipath1}, and figure \ref{multipath2} shows, we plot the cluster centroid in a 2-D plane.
The position of the green point is almost static. In contrast, the red point position indicates that multipath is change quick and unstable.
Thus we can include the estimation from the previous round of estimation to help identify the target point in the current timestamp by choosing the point that has the smallest distance from the previous estimate. 
We apply the L1-distance to quantify the distance between the points.
\begin{equation}
	L1 = \left|{r}_{t}-{r}_{t-1}\right|+\left|{v}_{t}-{v}_{t-1}\right|+\left|{\theta}_{t}-{\theta}_{t-1}\right|+\left|{\tau}_{t}-{\tau}_{t-1}\right|
\end{equation}
We will measure the L1-distance between each two consecutive points on different time.
The  two consecutive points  with minimum L1 distance is chosen as the best match for the trace each targets.
The multipath points  would have a relatively large L1-distance, thus would be  eliminated. 
After the process of the L1-distance, we will get a set of traces that corresponding to the targets movement.
In one target scenario, only one trace would remain.
In multiple targets scenario, then there would be multiple traces.



