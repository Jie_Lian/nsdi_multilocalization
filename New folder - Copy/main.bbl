\begin{thebibliography}{10}

\bibitem{adib2015multi}
Fadel Adib, Zachary Kabelac, and Dina Katabi.
\newblock Multi-person localization via $\{$RF$\}$ body reflections.
\newblock In {\em 12th $\{$USENIX$\}$ Symposium on Networked Systems Design and
  Implementation ($\{$NSDI$\}$ 15)}, pages 279--292, 2015.

\bibitem{adib2014witrack}
Fadel Adib, Zachary Kabelac, Dina Katabi, and Rob Miller.
\newblock Witrack: motion tracking via radio reflections off the body.
\newblock In {\em Proc. of NSDI}, 2014.

\bibitem{aumi2013doplink}
Md~Tanvir~Islam Aumi, Sidhant Gupta, Mayank Goel, Eric Larson, and Shwetak
  Patel.
\newblock Doplink: Using the doppler effect for multi-device interaction.
\newblock In {\em Proceedings of the 2013 ACM international joint conference on
  Pervasive and ubiquitous computing}, pages 583--586, 2013.

\bibitem{ayyalasomayajula2020deep}
Roshan Ayyalasomayajula, Aditya Arun, Chenfeng Wu, Sanatan Sharma,
  Abhishek~Rajkumar Sethi, Deepak Vasisht, and Dinesh Bharadia.
\newblock Deep learning based wireless localization for indoor navigation.
\newblock In {\em Proceedings of the 26th Annual International Conference on
  Mobile Computing and Networking}, pages 1--14, 2020.

\bibitem{chen2020tagray}
Ziyang Chen, Panlong Yang, Jie Xiong, Yuanhao Feng, and Xiang-Yang Li.
\newblock Tagray: Contactless sensing and tracking of mobile objects using cots
  rfid devices.
\newblock In {\em IEEE INFOCOM 2020-IEEE Conference on Computer
  Communications}, pages 307--316. IEEE, 2020.

\bibitem{fletcher2018effects}
Mark~D Fletcher, Sian Lloyd~Jones, Paul~R White, Craig~N Dolder, Timothy~G
  Leighton, and Benjamin Lineton.
\newblock Effects of very high-frequency sound and ultrasound on humans. part
  ii: A double-blind randomized provocation study of inaudible 20-khz
  ultrasound.
\newblock {\em The Journal of the Acoustical Society of America},
  144(4):2521--2531, 2018.

\bibitem{gu2019mmsense}
Tianbo Gu, Zheng Fang, Zhicheng Yang, Pengfei Hu, and Prasant Mohapatra.
\newblock Mmsense: Multi-person detection and identification via mmwave
  sensing.
\newblock In {\em Proceedings of the 3rd ACM Workshop on Millimeter-wave
  Networks and Sensing Systems}, pages 45--50, 2019.

\bibitem{huang2014shake}
Wenchao Huang, Yan Xiong, Xiang-Yang Li, Hao Lin, Xufei Mao, Panlong Yang, and
  Yunhao Liu.
\newblock Shake and walk: Acoustic direction finding and fine-grained indoor
  localization using smartphones.
\newblock In {\em IEEE INFOCOM 2014-IEEE Conference on Computer
  Communications}, pages 370--378. IEEE, 2014.

\bibitem{predict_indoor}
IndustryArc.
\newblock Indoor positioning and navigation market - forecast(2020 - 2025),
  2017.

\bibitem{jin2006indoor}
Guang-yao Jin, Xiao-yi Lu, and Myong-Soon Park.
\newblock An indoor localization mechanism using active rfid tag.
\newblock In {\em IEEE International Conference on Sensor Networks, Ubiquitous,
  and Trustworthy Computing (SUTC'06)}, volume~1, pages 4--pp. IEEE, 2006.

\bibitem{joshi2015wideo}
Kiran Joshi, Dinesh Bharadia, Manikanta Kotaru, and Sachin Katti.
\newblock Wideo: Fine-grained device-free motion tracing using $\{$RF$\}$
  backscatter.
\newblock In {\em 12th $\{$USENIX$\}$ Symposium on Networked Systems Design and
  Implementation ($\{$NSDI$\}$ 15)}, pages 189--204, 2015.

\bibitem{leighton2016some}
TG~Leighton.
\newblock Are some people suffering as a result of increasing mass exposure of
  the public to ultrasound in air?
\newblock {\em Proceedings of the Royal Society A: Mathematical, Physical and
  Engineering Sciences}, 472(2185):20150624, 2016.

\bibitem{leong2013multiple}
Pei~H Leong, Thushara~D Abhayapala, and Tharaka~A Lamahewa.
\newblock Multiple target localization using wideband echo chirp signals.
\newblock {\em IEEE Transactions on Signal Processing}, 61(16):4077--4089,
  2013.

\bibitem{ling2020ultragesture}
Kang Ling, Haipeng Dai, Yuntang Liu, Alex~X Liu, Wei Wang, and Qing Gu.
\newblock Ultragesture: Fine-grained gesture sensing and recognition.
\newblock {\em IEEE Transactions on Mobile Computing}, 2020.

\bibitem{lu2018lippass}
Li~Lu, Jiadi Yu, Yingying Chen, Hongbo Liu, Yanmin Zhu, Yunfei Liu, and Minglu
  Li.
\newblock Lippass: Lip reading-based user authentication on smartphones
  leveraging acoustic signals.
\newblock In {\em IEEE INFOCOM 2018-IEEE Conference on Computer
  Communications}, pages 1466--1474. IEEE, 2018.

\bibitem{mao2016cat}
Wenguang Mao, Jian He, and Lili Qiu.
\newblock Cat: high-precision acoustic motion tracking.
\newblock In {\em Proceedings of the 22nd Annual International Conference on
  Mobile Computing and Networking}, pages 69--81, 2016.

\bibitem{mao2019rnn}
Wenguang Mao, Mei Wang, Wei Sun, Lili Qiu, Swadhin Pradhan, and Yi-Chao Chen.
\newblock Rnn-based room scale hand motion tracking.
\newblock In {\em The 25th Annual International Conference on Mobile Computing
  and Networking}, pages 1--16, 2019.

\bibitem{matz2014effects}
Carlyn~J Matz, David~M Stieb, Karelyn Davis, Marika Egyed, Andreas Rose,
  Benedito Chou, and Orly Brion.
\newblock Effects of age, season, gender and urban-rural status on
  time-activity: Canadian human activity pattern survey 2 (chaps 2).
\newblock {\em International journal of environmental research and public
  health}, 11(2):2108--2124, 2014.

\bibitem{mirshekari2018occupant}
Mostafa Mirshekari, Shijia Pan, Jonathon Fagert, Eve~M Schooler, Pei Zhang, and
  Hae~Young Noh.
\newblock Occupant localization using footstep-induced structural vibration.
\newblock {\em Mechanical Systems and Signal Processing}, 112:77--97, 2018.

\bibitem{nandakumar2016fingerio}
Rajalakshmi Nandakumar, Vikram Iyer, Desney Tan, and Shyamnath Gollakota.
\newblock Fingerio: Using active sonar for fine-grained finger tracking.
\newblock In {\em Proceedings of the 2016 CHI Conference on Human Factors in
  Computing Systems}, pages 1515--1525, 2016.

\bibitem{nandakumar2017covertband}
Rajalakshmi Nandakumar, Alex Takakuwa, Tadayoshi Kohno, and Shyamnath
  Gollakota.
\newblock Covertband: Activity information leakage using music.
\newblock {\em Proceedings of the ACM on Interactive, Mobile, Wearable and
  Ubiquitous Technologies}, 1(3):1--24, 2017.

\bibitem{ni2003landmarc}
Lionel~M Ni, Yunhao Liu, Yiu~Cho Lau, and Abhishek~P Patil.
\newblock Landmarc: Indoor location sensing using active rfid.
\newblock In {\em Proceedings of the First IEEE International Conference on
  Pervasive Computing and Communications, 2003.(PerCom 2003).}, pages 407--415.
  IEEE, 2003.

\bibitem{oohashi2000inaudible}
Tsutomu Oohashi, Emi Nishina, Manabu Honda, Yoshiharu Yonekura, Yoshitaka
  Fuwamoto, Norie Kawai, Tadao Maekawa, Satoshi Nakamura, Hidenao Fukuyama, and
  Hiroshi Shibasaki.
\newblock Inaudible high-frequency sounds affect brain activity: hypersonic
  effect.
\newblock {\em Journal of neurophysiology}, 2000.

\bibitem{orr2000smart}
Robert~J Orr and Gregory~D Abowd.
\newblock The smart floor: A mechanism for natural user identification and
  tracking.
\newblock In {\em CHI'00 extended abstracts on Human factors in computing
  systems}, pages 275--276, 2000.

\bibitem{sun2018vskin}
Ke~Sun, Ting Zhao, Wei Wang, and Lei Xie.
\newblock Vskin: Sensing touch gestures on surfaces of mobile devices using
  acoustic signals.
\newblock In {\em Proceedings of the 24th Annual International Conference on
  Mobile Computing and Networking}, pages 591--605, 2018.

\bibitem{sun2015widraw}
Li~Sun, Souvik Sen, Dimitrios Koutsonikolas, and Kyu-Han Kim.
\newblock Widraw: Enabling hands-free drawing in the air on commodity wifi
  devices.
\newblock In {\em Proceedings of the 21st Annual International Conference on
  Mobile Computing and Networking}, pages 77--89, 2015.

\bibitem{tan2017silenttalk}
Jiayao Tan, Cam-Tu Nguyen, and Xiaoliang Wang.
\newblock Silenttalk: Lip reading through ultrasonic sensing on mobile phones.
\newblock In {\em IEEE INFOCOM 2017-IEEE Conference on Computer
  Communications}, pages 1--9. IEEE, 2017.

\bibitem{thinsungnoena2015clustering}
Tippaya Thinsungnoena, Nuntawut Kaoungkub, Pongsakorn Durongdumronchaib,
  Kittisak Kerdprasopb, and Nittaya Kerdprasopb.
\newblock The clustering validity with silhouette and sum of squared errors.
\newblock {\em learning}, 3(7), 2015.

\bibitem{van2014camera}
Dominik Van~Opdenbosch, Georg Schroth, Robert Huitl, Sebastian Hilsenbeck,
  Adrian Garcea, and Eckehard Steinbach.
\newblock Camera-based indoor positioning using scalable streaming of
  compressed binary image signatures.
\newblock In {\em 2014 IEEE International Conference on Image Processing
  (ICIP)}, pages 2804--2808. IEEE, 2014.

\bibitem{wang2019millisonic}
Anran Wang and Shyamnath Gollakota.
\newblock Millisonic: Pushing the limits of acoustic motion tracking.
\newblock In {\em Proceedings of the 2019 CHI Conference on Human Factors in
  Computing Systems}, pages 1--11, 2019.

\bibitem{wang2018low}
Ju~Wang, Jie Xiong, Hongbo Jiang, Kyle Jamieson, Xiaojiang Chen, Dingyi Fang,
  and Chen Wang.
\newblock Low human-effort, device-free localization with fine-grained
  subcarrier information.
\newblock {\em IEEE Transactions on Mobile Computing}, 17(11):2550--2563, 2018.

\bibitem{wang2013rf}
Jue Wang, Fadel Adib, Ross Knepper, Dina Katabi, and Daniela Rus.
\newblock Rf-compass: Robot object manipulation using rfids.
\newblock In {\em Proceedings of the 19th annual international conference on
  Mobile computing \& networking}, pages 3--14, 2013.

\bibitem{wang2013dude}
Jue Wang and Dina Katabi.
\newblock Dude, where's my card? rfid positioning that works with multipath and
  non-line of sight.
\newblock In {\em Proceedings of the ACM SIGCOMM 2013 conference on SIGCOMM},
  pages 51--62, 2013.

\bibitem{wang2014ubiquitous}
Junjue Wang, Kaichen Zhao, Xinyu Zhang, and Chunyi Peng.
\newblock Ubiquitous keyboard for small mobile devices: harnessing multipath
  fading for fine-grained keystroke localization.
\newblock In {\em Proceedings of the 12th annual international conference on
  Mobile systems, applications, and services}, pages 14--27, 2014.

\bibitem{wang2018c}
Tianben Wang, Daqing Zhang, Yuanqing Zheng, Tao Gu, Xingshe Zhou, and
  Bernadette Dorizzi.
\newblock C-fmcw based contactless respiration detection using acoustic signal.
\newblock {\em Proceedings of the ACM on Interactive, Mobile, Wearable and
  Ubiquitous Technologies}, 1(4):1--20, 2018.

\bibitem{wang2016device}
Wei Wang, Alex~X Liu, and Ke~Sun.
\newblock Device-free gesture tracking using acoustic signals.
\newblock In {\em Proceedings of the 22nd Annual International Conference on
  Mobile Computing and Networking}, pages 82--94, 2016.

\bibitem{wang2020push}
Yanwen Wang, Jiaxing Shen, and Yuanqing Zheng.
\newblock Push the limit of acoustic gesture recognition.
\newblock {\em IEEE Transactions on Mobile Computing}, 2020.

\bibitem{wei2015mtrack}
Teng Wei and Xinyu Zhang.
\newblock mtrack: High-precision passive tracking using millimeter wave radios.
\newblock In {\em Proceedings of the 21st Annual International Conference on
  Mobile Computing and Networking}, pages 117--129, 2015.

\bibitem{wei2017celli}
Yu-Lin Wei, Chang-Jung Huang, Hsin-Mu Tsai, and Kate Ching-Ju Lin.
\newblock Celli: Indoor positioning using polarized sweeping light beams.
\newblock In {\em Proceedings of the 15th Annual International Conference on
  Mobile Systems, Applications, and Services}, pages 136--147, 2017.

\bibitem{wixey2020feasibility}
Matt Wixey, Emiliano De~Cristofaro, and Shane~D Johnson.
\newblock On the feasibility of acoustic attacks using commodity smart devices.
\newblock In {\em 2020 IEEE Security and Privacy Workshops (SPW)}, pages
  88--97. IEEE, 2020.

\bibitem{xie2016lips}
Bo~Xie, Kongyang Chen, Guang Tan, Mingming Lu, Yunhuai Liu, Jie Wu, and Tian
  He.
\newblock Lips: A light intensity--based positioning system for indoor
  environments.
\newblock {\em ACM Transactions on Sensor Networks (TOSN)}, 12(4):1--27, 2016.

\bibitem{xu2020edge}
Jingao Xu, Hao Cao, Danyang Li, Kehong Huang, Chen Qian, Longfei Shangguan, and
  Zheng Yang.
\newblock Edge assisted mobile semantic visual slam.
\newblock In {\em IEEE INFOCOM 2020-IEEE Conference on Computer
  Communications}, pages 1828--1837. IEEE, 2020.

\bibitem{yang2020rf}
Panlong Yang, Yuanhao Feng, Jie Xiong, Ziyang Chen, and Xiang-Yang Li.
\newblock Rf-ear: Contactless multi-device vibration sensing and identification
  using cots rfid.
\newblock In {\em IEEE INFOCOM 2020-IEEE Conference on Computer
  Communications}, pages 297--306. IEEE, 2020.

\bibitem{yuan2014localization}
Qilong Yuan and I-Ming Chen.
\newblock Localization and velocity tracking of human via 3 imu sensors.
\newblock {\em Sensors and Actuators A: Physical}, 212:25--33, 2014.

\bibitem{yun2015turning}
Sangki Yun, Yi-Chao Chen, and Lili Qiu.
\newblock Turning a mobile device into a mouse in the air.
\newblock In {\em Proceedings of the 13th Annual International Conference on
  Mobile Systems, Applications, and Services}, pages 15--29, 2015.

\bibitem{yun2017strata}
Sangki Yun, Yi-Chao Chen, Huihuang Zheng, Lili Qiu, and Wenguang Mao.
\newblock Strata: Fine-grained acoustic-based device-free tracking.
\newblock In {\em Proceedings of the 15th annual international conference on
  mobile systems, applications, and services}, pages 15--28, 2017.

\bibitem{zhang2017soundtrak}
Cheng Zhang, Qiuyue Xue, Anandghan Waghmare, Sumeet Jain, Yiming Pu, Sinan
  Hersek, Kent Lyons, Kenneth~A Cunefare, Omer~T Inan, and Gregory~D Abowd.
\newblock Soundtrak: Continuous 3d tracking of a finger using active acoustics.
\newblock {\em Proceedings of the ACM on Interactive, Mobile, Wearable and
  Ubiquitous Technologies}, 1(2):1--25, 2017.

\bibitem{zhang2016litell}
Chi Zhang and Xinyu Zhang.
\newblock Litell: Robust indoor localization using unmodified light fixtures.
\newblock In {\em Proceedings of the 22nd Annual International Conference on
  Mobile Computing and Networking}, pages 230--242, 2016.

\bibitem{zhang2020your}
Fusang Zhang, Zhi Wang, Beihong Jin, Jie Xiong, and Daqing Zhang.
\newblock Your smart speaker can" hear" your heartbeat!
\newblock {\em Proceedings of the ACM on Interactive, Mobile, Wearable and
  Ubiquitous Technologies}, 4(4):1--24, 2020.

\bibitem{zhang2017hearing}
Linghan Zhang, Sheng Tan, and Jie Yang.
\newblock Hearing your voice is not enough: An articulatory gesture based
  liveness detection for voice authentication.
\newblock In {\em Proceedings of the 2017 ACM SIGSAC Conference on Computer and
  Communications Security}, pages 57--71, 2017.

\bibitem{zhang2021soundlip}
Qian Zhang, Dong Wang, Run Zhao, and Yinggang Yu.
\newblock Soundlip: Enabling word and sentence-level lip interaction for smart
  devices.
\newblock {\em Proceedings of the ACM on Interactive, Mobile, Wearable and
  Ubiquitous Technologies}, 5(1):1--28, 2021.

\bibitem{zhang2020endophasia}
Yongzhao Zhang, Wei-Hsiang Huang, Chih-Yun Yang, Wen-Ping Wang, Yi-Chao Chen,
  Chuang-Wen You, Da-Yuan Huang, Guangtao Xue, and Jiadi Yu.
\newblock Endophasia: Utilizing acoustic-based imaging for issuing contact-free
  silent speech commands.
\newblock {\em Proceedings of the ACM on Interactive, Mobile, Wearable and
  Ubiquitous Technologies}, 4(1):1--26, 2020.

\bibitem{zhao2019mid}
Peijun Zhao, Chris~Xiaoxuan Lu, Jianan Wang, Changhao Chen, Wei Wang, Niki
  Trigoni, and Andrew Markham.
\newblock mid: Tracking and identifying people with millimeter wave radar.
\newblock In {\em 2019 15th International Conference on Distributed Computing
  in Sensor Systems (DCOSS)}, pages 33--40. IEEE, 2019.

\bibitem{zhu2015reusing}
Yanzi Zhu, Yibo Zhu, Ben~Y Zhao, and Haitao Zheng.
\newblock Reusing 60ghz radios for mobile radar imaging.
\newblock In {\em Proceedings of the 21st Annual International Conference on
  Mobile Computing and Networking}, pages 103--116, 2015.

\end{thebibliography}
