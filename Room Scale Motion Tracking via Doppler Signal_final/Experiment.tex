\section{EXPERIMENT}
We have implemented our   motion tracking system for conducting extensive experiments to evaluate its performance. 



\subsection{Experiment Setup} 
Our system comprises   a ReSpeaker 4-Mic Linear Array, which is connected to a Raspberry Pi 4 and a co-located Edifier R1280DB speaker.
Note that  smart devices do  not release their APIs, so we cannot directly program their microphones for our purpose.
Instead, we are using the ReSpeaker 4-Mic Linear Array, which has a   layout similar to those of  current smart  devices.
This microphone array has four microphones, with the distance between two adjacent microphones is  $5.08cm$.
We place the microphone array atop the speaker to ensure that both the microphone and the speaker are located in the same place.
When our system is triggered, the speaker is controlled to send  continuous sinusoid wave at $20kHz$ with a sampling frequency of $44.1kHz$.
The transmission power is tuned to  80\% of the maximum volume, so the measured power at 1 meter away from the speaker is 45dB. 
The microphone array records reflected signals, which are saved via the Raspberry Pi 4 at the sampling rate of $44.1kHz$.
The Raspberry Pi 4 is connected to a laptop via   Wi-Fi connection and all signals are processed via Matlab in this laptop.
We measure the located positions of moving persons in the 2-D plane, by creating their corresponding trajectories on the floor to serve as the ground truth. 
The localization error is used as our evaluation metric, defined by the Euclidean distance between each located position and its ground truth coordinate. 





\subsection{Performance on Locating a Single Moving Person} 

In this experiment, we let a person  walk at his natural speed along the predefined trajectory.
Our system processes the reflected signals to get this person's location at each 0.1s.
The localization error in each 0.1s is calculated, with the CDF result depicted in Figure~\ref{movingstatic}. 
This figure reveals that  the localization error of 40$\%$ (or 80$\%$) positions is less than $5.4cm$  (or $12.5 cm$),  with the mean localization error equal to $7.49 cm$.
Such results are promising and  demonstrate that our system can accurately track a person's moving trace. 



Further experiments are conducted to evaluate the localization errors of different distances away from the speaker.
We truncate the collected data at five distance ranges, i.e., $(1m\sim 2m), (2m \sim 3m), (3m \sim 4m), (4m \sim 5m)$ and $(5m \sim 6m)$, indicating by $1m$, $2m$, $3m$, $4m$, and $5m$.
Figure~\ref{rangeimpact} shows results of mean errors for distances of $1m$, $2m$, $3m$, $4m$, and $5m$, equal to $3.6cm, 4.3cm, 7.9cm, 13.7cm,  $ and $21.09cm$, respectively.
Obviously, a larger distance leads to a bigger error, as expected since the acoustic signals experience  fast attenuation when the distance increases. 
The reflected signals are weakened for a bigger distance, causing Doppler signals harder to be extracted and thus yielding bigger errors in calculating  phase and velocity to deteriorate performance. 
Nonetheless,  the maximum error of $21.09cm$ at the range of $(5m-6m)$  is far smaller than the human body size, making our system suffice for use in the room scale environment.


\begin{figure*}
	\centering	
	\begin{minipage}{0.43\textwidth}
		\centering
		\vspace{0.8em}
		\includegraphics[width=2.4in]{fig/_movingstatic.pdf}
		\caption{Localization errors when locating a single person.}
		\label{movingstatic}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.43\textwidth}
		\centering
		\includegraphics[width=2.4in]{fig/_range.pdf}
		\caption{Localization errors at different ranges.}
		\label{rangeimpact}
	\end{minipage}
\end{figure*}





\subsection{Robustness for  Single Person Localization} 
To show the robustness of our system, we take into account different factors and examine their impacts on our system performance. 



\smallskip
\noindent\textbf{Impact of Moving Speeds.} \ 
We let a person walk at different   speeds for examining system performance.
Three different walking speeds are considered: 1) low speed (< $1m/s$), 2) normal speed (around $1m/s$), and 3) high speed. 
Figure~\ref{different_speed} shows the quartiles figure under the  three moving speeds.
From this figure, we observe our system to achieve the worse  performance when a person is moving at the low or high speed, in comparison to moving at the normal speed.
Specifically,  the averaged errors are $7.4cm, 10.1cm, $ and $13.2cm$ respectively, with respect to the normal speed, low speed, and high speed. 
The reason is as follows.
When a person is walking at the high speed, the Doppler effect is more apparent to make our system's assumption of a person's velocity  considered as a constant in a short time period to yield a large error. 
\begin{figure}[htb]
	\centering
	\includegraphics[width=2.4in]{fig/_different_speed2.pdf}
	\vspace{-0.6em}
	\caption{Impact of different speeds.}
	\label{different_speed}
\end{figure}
On the other hand, a slow walking speed weakens the Doppler effect, resulting in light narrowband signals for localization, thereby degrading system performance.







\smallskip
\noindent\textbf{Impact of Audible Sounds.} \ 
Since our system works at the $20kHz$, inaudible to human, we next examine the possible impacts of pervasively existing audible sounds. 
Two audible sounds common in the home environments are considered: 1) humans talking and 2) music playing. 
The   two sound sources are placed at $0.5m$ away from the speaker. 
Figure~\ref{diff_noise} shows the performance of our system under different scenarios, where {\em silent} indicates that  no audible sound exists in the environment.
When comparing three scenarios, we can observe the audible sounds (i.e., human talking and music playing) only have  slight impacts  on system performance.
The averaged errors in a silent environment,  a human talking environment, and a music playing environment are $7.4cm, 9.3cm, $ and $10.4cm$, respectively.
The reason is that the two audible sounds do not generate any frequency component in the ultrasound domain, without causing interference to our system.
The slight performance difference comes from  frequency leakage caused by imperfect STFT, which produces certain unexpected high-frequency components,  interfering with the target signal.
But such frequency leakage typically is insignificant due to the use of continuous wave, yielding a minor impact.

\begin{figure}[htb]
	\centering
	\includegraphics[width=2.3in]{fig/_different_noise.pdf}
		\vspace{-0.6em}
	\caption{Impact of audible sounds.}	
	\label{diff_noise}
\end{figure}



\smallskip
\noindent\textbf{Impact of Different Devices.} \ 
We next show our system is transferable to different devices.
Three different speakers are examined, i.e.,  Edifier R1280DB, Logitech z200, and Amazon Echo, indicated respectively as  Speaker 1, Speaker 2, and Speaker 3. 
In our experiment,   speakers' volumes are tuned to the same level and we let them emit the $20kHz$ continuous wave.
Our system performance results of those three different speakers are shown in Figure~\ref{Diff_speaekr}.
We observe that   Speakers 2 and 3 underperform Speaker 1.
\begin{figure}[htb]
	\centering
	\includegraphics[width=2.4in]{fig/_different_speaker.pdf}
	\vspace{-0.6em}
	\caption{Performance with different speakers.}
	\label{Diff_speaekr}
\end{figure}
The reason is that Amazon Echo  generates signals in all directions, making their reflected signals have relatively low power to result in a low SNR. 
Logitech z200 is the cheapest device, so its generated signals have lower quality than that of Edifier R1280DB's signals.
However, their performance levels are still acceptable, i.e., $80\%$ of localization errors being less than $18.8cm$.
Overall, the averaged localization errors of Speaker 1, Speaker 2, and Speaker 3 are  $7.4cm, 11.7cm, $ and $12.1cm$, respectively.
Such  experimental results demonstrate the good transferability of our system to different smart home devices. 


\smallskip
\noindent\textbf{Impact of Different Environments.} \ 
We next conduct experiments in three rooms with different layouts to quantify the robustness of our system, with  experimental results demonstrated in Figure~\ref{different_env}.
Our system is found to achieve the similar performance in  three rooms, with the mean localization errors of $7.4cm,  9.6cm, $ and $9.0cm$, respectively. 
This is due to the fact that reflections from all environments have  the same frequency as the originally transmitted signal, which is fully eliminated via our noise cancellation step.
It is thus concluded  that our system is robust to the environmental changes. 


\begin{figure}[htb]
	\centering
	\includegraphics[width=2.3in]{fig/_different_env2.pdf}
			\vspace{-0.6em}
	\caption{Impact of different environment.}
	\label{different_env}
\end{figure}


\subsection{Tracking Multiple Moving Persons} 






Our system is also applicable for locating multiple moving persons.
We conduct experiments to show its performance when multiple moving persons co-exist in a room. 





\smallskip
\noindent\textbf{Localization Performance}. Two persons  walk on two predefined trajectories (i.e., straight lines) separated by $2$m.
\begin{figure}[htb]
	\centering
	\includegraphics[width=2.4in, clip]{fig/_twomoving.pdf}
	\vspace{-0.6em}
	\caption{Localization errors when locating two moving persons.}
	\label{twomoving}
\end{figure}
Figure \ref{twomoving} shows the CDFs of localization errors under two moving persons, with mean errors equal to  $22.6cm$ and $25.4cm$, respectively.
When comparing to Figure~\ref{movingstatic}, we find the performance results  are degraded   since some Doppler signals from two bodies  have identical frequencies, overlapped in the same narrowband and making them hard to be separated. 
Since the maximum error is less than $50cm$, our system is accurate enough for tracking two persons' traces. 










\smallskip
\noindent\textbf{Impact of Distances between Two Targets.} \ 
The impact of the distance separating two persons is assessed. 
We let two persons be separated $3m, 2m,  1m$, and $0.5m$, respectively, when walking towards the speaker from $5m$ away to $1m$ away. 
Figure~\ref{Diffdistance} plots the CDF curves of localization errors under various separation distances. 
We  observe that our system performance degrades as the separation distance drops. 

Our system performs the best at the separation distance of $3m$, with the averaged errors of $21.2cm, $ and $21cm$, respectively, for the two persons.
Under the separation distance of $0.5m$, our system performs the worst, with the mean errors of $102cm$ and $99cm$, respectively. 
The reason is that when two persons are closer to each other, their mutual interference hike, making our system harder to cluster them. 

\begin{figure}[htb]
	\centering
	\includegraphics[width=2.4in, clip]{fig/_diffdistance.pdf}
	\caption{Localization errors under different separation distances.}
	\label{Diffdistance}
\end{figure}




\smallskip
\noindent\textbf{Performance under Different Target Counts}. \ 
We increase the number of targets from 1 to 3 for examining
 the system performance results. 
 Three sets of experiments are conducted, respectively under   single person, two persons, and three persons.
In each experimental set, every target walks towards the speaker at his/her natural speed.
Figure~\ref{Difftarget} shows the CDF curves of localization errors with respect to the three target counts, with  different color curves denoting the three target counts.
\begin{figure}[htb]
	\centering
	\includegraphics[width=2.4in, clip]{fig/_three.pdf}
	\caption{Localization errors on different target counts.}
	\label{Difftarget}
\end{figure}
Their averaged errors are $7.4cm, 24.1cm, $ and $ 51.2cm$, respectively.  
It is observed that our system performance degrades faster with a larger target count. 
Especially, with three targets, more than $40\%$ of measured points have their localization errors exceeding $50cm$. 
The reason is that more targets  increase the chance that Doppler signals from different targets overlap in the same narrowband and incur more complex multipath reflections, making it harder to separate signals from different persons. 
Nonetheless, the localization errors are still less than $1m$, useful for coarse location tracking. 





\subsection{Extension to Locate a Stationary Person}

Since our system relies on the phase change of Doppler signals for localization, it cannot localize a stationary object. 
However, we can have an aid of arm swings to localize a stationary person. 
To this end, an experiment is undertaken to gauge the performance of localizing static targets, by letting the participants   stand at predefined points with their arms swing.
Figure~\ref{statics} depicts the CDF curves, with the red curve (or blue curve) indicating the mean localization errors of a single person (or two persons).
When comparing to Figures~\ref{movingstatic} and \ref{twomoving} (respectively for locating the  single moving person and two moving persons), we find degraded performance.
This is due to the fact that the reflections from arm swings are much weaker than those from the entire body segments, giving rise to inferior performance. 
However, the mean errors are of  $21.1cm$ (or $35.4cm$) for a single person (or two persons).
Such results are still promising, suggesting that our system is applicable to track the movement of a relatively small  target or fine-grained movement, such as gesture tracking. 

\begin{figure}[htb]
	\centering
	\includegraphics[width=2.4in, clip]{fig/_statics.pdf}
	\caption{Localization errors when locating the stationary persons.}
	\label{statics}
\end{figure}

























