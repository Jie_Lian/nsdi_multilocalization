\section{Introduction}


The indoor localization has spurred extensive personalized applications. 
The prior report has revealed that a person may spend almost $88.9\%$ of the day indoors~\cite{matz2014effects}.
Recently, \cite{predict_indoor} has predicted that the market value of indoor positioning and indoor navigation is expected to exceed  $23.6$ billion dollars in 2023, suggesting the tremendous demand for reliable indoor localization technologies. 
Among the emerging technologies, the device-free indoor localization without requiring a user to carry any device for sensing, is most appealing in many scenarios, prompting diverse applications such as smart homes, healthcare services, fitness tracking, etc. 
The acoustic-based device-free sensing approach is  promising and affordable, given the increasing prevalence of in-house smart devices. 
Its low frequency range sensing is readily achieved by the  commercial off-the-shelf (COTS) devices. 
Various human-computer interaction services based on the acoustic signal have been exploited, such as  activity recognition \cite{ling2020ultragesture, wang2020push, lian2021fall},  lip-reading \cite{zhang2020endophasia, zhang2021soundlip, tan2017silenttalk},  respiration and heartbeat detection \cite{zhang2020your, wang2018c}, localization \cite{mao2019rnn, li2020fm,lian2021echospot}, etc. 
However, it remains challenging  to develop a reliable system for  room-scale location trace in real home environments. 



Although previous studies in device-free acoustic sensing have achieved the millimeter level accuracy~\cite{nandakumar2016fingerio, wang2016device} and extended the sensing range to $4.5m$~\cite{mao2019rnn}, they mainly strive to explore inaudible chirp signals, which  typically entice two fundamental issues. 
First, chirp signals require to have   short durations and large bandwidth for achieving satisfactory performance. 
However, when the  COTS devices are controlled to generate a short  chirp signal of wide bandwidth  under the inaudible frequency, the electronic burst can occasionally cause an audible ``Beep'' sound on the speaker, considered to be rather annoying to humans. 	
In addition, due to the limited space of an indoor environment, the chirp signal will be reflected multiple times before it completely attenuates, resulting in multiple echoes.	
As these echoes are amenable to environmental changes, any human motion may alter the echoes significantly.	
Therefore, the echoes of chirps can be different, difficult to be eliminated completely, thus degrading the signal quality and incurring the range estimation error.
In sharp contrast to chirp signals, the continuous waves emitted by  COTS devices do not generate the perceptible sounds to the human as they operate in the inaudible frequency. 
In addition, the frequencies of reflections from stationary objects in an environment are relatively stable, causing little interference to  target signals   reflected from a moving person, making it possible to eliminate those static reflection frequencies. 




In this paper, we   explore the continuous wave as the sensing signal and leverage the phase change of Doppler signals for tracking the motions of moving targets. 
Although   device-free tracking based on the phase measurement has been pursued previously~\cite{wang2016device, yun2017strata, sun2018vskin}, the sensing ranges achieved therein are considerably short (only up to 1 meter), unsuitable for   location trace tracking in a room. 
This is due to the fact that they ignored the Doppler effect of  signal and directly extracted the phase from the reflected signals by referring to the original signal as the baseline.	
Albeit simple, such a method can be effective just for a short distance.	
However, in a room-scale environment, the original signal will be transmitted over multiple paths to the microphone.	
The multiple path signals can be viewed as several time-delay versions of the original signal, and after those signals are aggregated, the original signal experiences phase distortion.
Thus applying the original signal as the baseline tends to yield wrong phase estimation in a room-scale environment.
Instead, we examine the phase change of reflected Doppler signals, permitting to acquire the proper baseline signal  from the body reflection signals.
Since this way makes it possible  to obtain very accurate phase estimation over a relatively large range, the sensing range can thus be significantly extended  for applications to room-scale environments.







To this end, we strive to develop a novel device-free indoor localization system by relying on continuous waves emitted from the speaker's built-in smart devices and performing the fine-grained phase and frequency analyses of  Doppler signals received at the built-in microphone array to implement precise indoor location trace tracking. 
Specifically, we control a speaker to generate the continuous wave ultrasound at $20kHz$ and leverage a co-located microphone array to record  reflected signals for analysis.  
We  first perform the Short-Time Fourier Transform (STFT) to get the spectrogram of reflected Doppler signals and then apply a set of interference cancellation techniques to remove the interference/noise caused by both  surrounding objects and system defects.
Considering different body segments of a moving person may generate Doppler signals at different frequencies, we propose a new solution by slicing signals into a set of narrowband signals, with each narrowband likely to include the reflections from only one  body segment. 
We next perform the fine-grained analysis on each narrowband signal by  a series of developed solutions. 
First, we estimate the velocity via analyzing the original signal from the speaker and the received signal at the microphone array, followed by utilizing such a velocity to estimate the baseband frequency of the narrowband signal. 
The I-Q (i.e., in-phase and quadrature) decomposition is then applied to accurately measure its phase.
After that, a signal model is  proposed to formulate the phase correlated to the range, angle, and velocity.
The range and angle of each narrowband signal are then estimated by solving an optimization problem, aiming to get each body segment's motion state.
Next, a solution based on   K-mean clustering is applied to cluster the motion states associated with a given person. 
Finally, we apply the L1-norm (i.e., the sum of the magnitudes of the vectors) to refine our estimation and remove the interference from  multi-path effects, able to get the number of moving persons and their associated location traces accurately. 







Our contributions are summarized as follows.
\begin{itemize} 
	\item We develop a new device-free localization system, which leverages the continuous wave emitted from the smart speaker as the sensing signal for precise localization.
As far as we know, this study is the very first to demonstrate the possibility of tracking multiple individuals using continuous waves.
To achieve our goal, we develop solutions to perform the fine-grained analysis of Doppler signals by extracting their frequency and phase details for localization. 
Unlike the chirp-based localization counterparts, our system only relies on continuous waves at $20kHz$, which utilize a small bandwidth and are both imperceptible to humans and robust to environmental factors.  



\item Novel solutions to enable the phase measurement of Doppler signals are developed by slicing
	the Doppler signals into narrowband signals to have  each narrowband contain  the reflection of a single body segment  for the fine-grained analysis of human body patterns. 
	So, the narrowband signals can be viewed as single tone signals, enabling to estimate the
	phase of each narrowband signal determined by estimating its baseband signal.
	We are the first   to extract the phase of a Doppler signal by dividing it into narrowband signals, which can facilitate the fine-grained analysis for multiple-person location tracing. 
	A new signal model is then proposed for each narrowband signal to fuse the target velocity, range, and angle, enabling the estimation of the motion states of different body segments.
	Motion state clustering and location trace solutions are also proposed to precisely group together reflections from different body segments of the same person and to further eliminate the multi-path effect.  





\item We implement our system with COTS devices and conduct extensive experiments for performance evaluation. 
The experimental results exhibit that our system: 1)  can achieve the precise location tracking for a single person, with an average error of $7.49cm$; 2) can support the multi-target tracking, having the average errors of $24.06cm$ and of $51.15cm$, respectively, for two and three persons; 3)  is robust to various environmental factors;  4) can be extended for locating a stationary person with a swing arm.
	
	

	
\end{itemize}
















