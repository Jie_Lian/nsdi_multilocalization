\section{Problem Statement}

This paper aims to develop a new device-free localization system via inaudible acoustic sensing by harnessing pervasively available commercial off-the-shelf (COTS) smart home devices. 
The speaker of a smart device is controlled to emit the inaudible acoustic signals at $20kHz$, while the built-in microphone array receives the reflected signals from the moving target for analyses in order to track its moving trace. 
Although the chirp signal-based solutions have been extensively pursued for localization, they have  inherent drawbacks. 
First, the COTS devices are originally manufactured for sending the continuous waves, but when controlled to generate the chirp signals in the inaudible frequency band (i.e., $18kHz$ to $23kHz$), the electric burst at the speaker may bump noise audible to a human.
Such noise sounds like ``Beep'', which can annoy people's life. 
Second, the chirp signals require  large bandwidth for accurate tracking~\cite{mao2016cat}, but their associated ultrasonic signal is more likely to be heard by some sensitive people.
Third, due to the frequency  fluctuation of chirp signals, their reflection echoes from the environment will overlap with those from the moving target, making them hard to be differentiated  and thus incurring  potential location estimation errors.


\subsection{Our Goal and Challenges}
To overcome the limitations of chirp-based localization, we control the speaker to emit the  continuous waves of inaudible acoustic signals while measuring the phase changes of Doppler shifts for tracking the walking trace of a human in  room-scale environments. 
Our approach is expected to have three salient features when compared with  chirp-based methods. 
First, the continuous waves are generated by following the original design of COTS devices, so their controlled transmissions in the inaudible frequency will not generate audible noise. 
Second, the static environment reflection will have a relatively stable frequency, which can be easily differentiated from  shifted frequencies caused by a moving target. 
Third, the distance resolution is superior, given that the phase measurement can track the subtle distance variation via a small phase shift. 
Besides,  analyzing the phase shift of  Doppler signals can eliminate any potential error caused by the Doppler effect. 
Nonetheless, a set of technical challenges surfaces in designing such a system for deployment in home environments,  outlined as follows. 


\begin{itemize}
	\item The previous solutions for finger, hand, or chest tracking  by measuring the phase change of continuous waves, are inapplicable here since they consider the original signal as a baseband signal. 
	However,  the human moving often has a speed high enough to cause the clear Doppler effect of  original signal.
	For example,  a continuous acoustic wave with a  frequency of $f_{c}=20 \mathrm{kHz}$ reflected by a walking person with a speed of $v=0.5m/s$ incurs $29Hz$ Doppler shift on the baseband signal. 
	Such a frequency shift will cause a considerable error in   phase estimation, if  the original signal is considered as a baseband signal. 
	Hence, it is necessary  to design new methods toward identifying the shifted baseband signals for accurate phase estimation. 


	\item Different body segments will generate various Doppler shifts, with their phases mixed at the receiver side. 
	It is challenging to isolate them and identify the respective baseband signal of each body segment, while grouping those shift phases associated with the same person in a room where multiple persons exist.

	
	
	\item The multipath reflections will cause strong interference with target signals of interest in  room environments and they fluctuate with the movement of a target.
	It is challenging  to mitigate  multi-path effects, calling for new solutions for differentiating the target reflections. 

	
	
	
\end{itemize}




This paper aims to overcome the aforementioned challenges and develop the first continuous wave-based localization system based on the phase measurement, to be deployable for use in  home environments.
We briefly outline how we overcome the challenges mentioned above.

\begin{itemize}
	\item To address the first challenge, our proposed system will take into account the Doppler shift as the baseband frequency. We achieve this by calculating the velocity of the body for determining the associated Doppler shift, and then combining it with the original frequency emitted by the speaker to serve as the baseband frequency.	
	
	
	
	\item To tackle the second challenge, we'll segment the baseband signal into a series of narrowband signals, based on the fact that each narrowband is likely to contain reflections from at most one body segment. Subsequently, we compute the motion states, encompassing velocity, range, and angle of arrival, for the body segments within these narrowband signals. Ultimately, a clustering algorithm is employed to group together reflection signals originating from the same individual.
	
	
	\item The third challenge will be addressed by spectral subtraction, where we record the background reflection of the environmental objects and then subtract it from the recorded signals to get a clean signal of the human reflection.
\end{itemize}

Our solution is expected to  work  not only for the single person tracking, but also applicable for tracking multiple persons. 
Before presenting our design details in the next section, we briefly provide some preliminary knowledge relevant to our design next. 





\subsection{Preliminary Knowledge}

\noindent{\bf Parameters of Human Motion.}
To track the moving trace of a human, the following parameters are needed: 1) {\em Range,} indicating the absolute distance between the target and the device; 
2)  {\em Velocity,} representing the moving speed of a target (with a target signifying one body segment);
3) {\em Angle-of-Arrival (AoA),} denoting the angle of a target corresponding to the device. 







\noindent{\bf Doppler Effect.}
In our system, the movement of a target will generate the frequency shift of the original signal, which can be captured for sensing its motion status. 
The frequency of  Doppler signal is determined by the signal frequency and velocity of the signal transmitter and receiver.


If the signal transmitter is stationary and the receiver is moving at a velocity of $v$,  the frequency of its Doppler shift can be represented as:  
\begin{equation}
	f_{1}=\frac{c+v}{c}f_{0}, \label{doppler_formula1}
\end{equation}
where $c$ is the sound propagation speed in the air.
If the signal receiver is stationary and the transmitter is moving at a velocity of $v$,  the frequency of its Doppler shift can be represented as:  
\begin{equation}
	f_{2}=\frac{c}{c-v}f_{0}.  \label{doppler_formula2}
\end{equation}
Hence, the motion state of the signal transmitter or receiver significantly impacts the Doppler frequency.
In our problem, a moving person can be seen as a moving signal receiver, if the signal reaches the body from the speaker, or as a virtual moving signal transmitter, if the signal is reflected from the body to the  microphone array. 
As such,  the Doppler frequency of a moving target can be expressed by:
\begin{equation}
	f_{v}=\frac{c+v }{c}\frac{c}{c-v }f_{0}= \frac{c+v }{c-v}f_{0}. \label{doppler_formula}
\end{equation} 


