\section{System Design}


\begin{figure*}
	\centering
	\includegraphics[width=6.2in]{Fig/systemdesigns.pdf}
	\caption{The workflow of our continuous waves-based localization system via inaudible acoustic sensing.}
	\label{systemdesign}
\end{figure*}

In this section, we elaborate our design of the continuous-wave based localization system. 
Figure~\ref{systemdesign} presents the workflow of our system, consisting of four component modules: {\em Sensing, Signal Processing, Signal Modeling}, and {\em Target Localization}.
The {\em Sensing } module  involves a speaker to generate the continuous wave at 20kHz and   a microphone array to receive the reflected signals.
In the {\em Signal Processing} module, we first perform the STFT on received signals to generate their spectrogram.
Then, we develop a solution to eliminate the interference signals that (1) come from the direct transmission and (2) are reflected from surrounding objects.
What remains is a clean spectrogram, which is next divided into a set of narrowband signals, aiming to segregate signal components that are reflected from different human body segments.  
The {\em Signal Modeling} employs a series of our developed solutions for estimating the baseband signal in each narrowband signal, calculating the velocity according to its frequency, and analyzing the phase change of each narrowband signal for obtaining the signal phase with the aid of range, angle of arrival (AoA), and velocity. 
Finally, the {\em Target Localization} module relies on our developed estimator for estimating the location parameters of body segments.
It applies K-means clustering and L1-norm to fuse the estimation of a moving person, with the multipath effect mitigated to get the moving trace of a target.
The  details of each design component are provided  next. 



\subsection{Sensing}
The speaker is controlled to keep sending the $20kHz$ continuous waves.
We select $20kHz$ because it is imperceptible by most people and is also large enough to produce a significant Doppler effect.   
The signal will be transmitted at the low power for saving energy while the microphone array keeps sensing the sound pressure between $19kHz$ and $21kHz$. 
Once it  senses the power level exceeding a certain threshold due to Doppler signals caused by a moving target, our system is triggered to transmit the high powered continuous wave at frequency $f_{0}=20kHz$ with received signals processed for localization.
The transmitted continuous wave can be represented as follows:
\begin{equation}
	F(t)=A \cos \left(2 \pi f_{0} t\right).
\end{equation}

The microphone array receives the Doppler shift signals, which are reflected from different body segments of a moving person, at a sampling frequency of $44.1kHz$. 
Such a sampling frequency ensures the reflected signals are  entirely reconstructed from the signals recorded by the microphone array, according to the Nyquist Sampling Theorem.






\subsection{Signal Processing}
The recorded signals via the microphone array are inputted to the {\em Signal Processing} module for eliminating the interference and  then split into a set of narrowband signals. 


\subsubsection{Short Time Fourier Transform Processing}

We apply the short-time Fourier transform (STFT) to generate the spectrogram of signals.
In particular, the signals are sliced by a set of small time windows with a length of 0.3s. 
Two consecutive time windows are overlapped with $95\%$, meaning  an $1s$ signal will be sliced into 66 bins.
Each small bin is multiplied by a Hamming window, and then a 21000-point Fast Fourier transform (FFT) is applied to each bin.
We divide the frequency into 21000 sub-bands, having a frequency resolution of  $1Hz$.


\subsubsection{Interference Cancellation}
A moving person can be considered as a cluster of moving body segments (e.g., head, torso, arms, and legs), generating a collection of disparate reflection signals to be received by the microphone array. 
Besides, the direct transmission signals from the speaker and reflections from  stationary objects are also   received by the microphone array.
Hence, the composite signals received by the microphone array can be modeled by:
\begin{equation}
\small
	\begin{aligned}
		R(t)=A\cos \left(2 \pi f_{0} t\right) \hspace{-0.1em}+ \hspace{-0.3em}\sum  \hspace{-0.1em}S_{r}	+ \sum \hspace{-0.1em}A_{n} \cos \hspace{-0.1em}\left(2 \pi f_{0} (t-\tau_{n})\right)+N(t), \label{mixx}
	\end{aligned}
\end{equation}
where $A \cos \left(2 \pi f_{0} t\right)$ is the signal directly transmitted from the speaker to the microphone array.
$\sum S_{r}$ represents the combined signal reflected from different moving body segments.
Since the move of each body segment causes a Doppler effect, the frequency of each $S_{r}$ differs among one another and also differs from $f_{0}$.   
The term $A_{n} \cos \left(2 \pi f_{c} (t-\tau_{n})\right)$ represents the signal reflected from surrounding  objects.
Since these objects are stationary, their reflections have the same frequency as the transmitting signal $f_{0}$.  
 $N(t)$ indicates the noise caused by system defects. 
In this step, we aim to eliminate three categories of signals: (1) the direct transmission signals, (2) signals reflected from surrounding stationary objects,   and (3) the noise.


Given the  first two categories of signals have the frequency of  $f_{0}=20kHz$, we can directly set the spectrogram power at $20kHz$ to 0 for elimination. 
However, in practice,  the smart home speaker is not designed to generate the high-frequency ultrasound, so the frequency actually fluctuates slightly over time.
We cannot  completely remove them  by  setting the spectrogram power at $20kHz$ to 0.
Our empirical studies exhibit that such signal fluctuation typically ranges from $19.99kHz$ to $20.01kHz$.
Hence, we  set the spectrogram power in this range to be 0 instead, for fully removing direct transmission signals and  reflections from surrounding objects.

On the other hand, the noise in the ultrasound domain is caused by system defects, seen as the points spreading over the spectrogram.
Such noise does not vary with time, allowing us to subtract it by spectral subtraction directly.
In particular, we let the microphone periodically record the noise in the static environment  and then subtract the latest recorded noise from the current signal.




\subsubsection{Signal Slicing }
\label{slicesignla}
After the above process, we  get a relatively clean spectrogram  for $\sum S_{r}(t)$, which  can be considered as the pure Doppler signal, expressed as 
\begin{equation}
	\begin{aligned}
		\sum S_{r}(t)=\sum \alpha_{m} \cos \left(2 \pi f_{m} (t-\tau_{m})\right), \label{1114}
	\end{aligned}
\end{equation}
where $f_{m}$ represents each baseband signal frequency and $\tau_{m}$ indicates its ToF.
Notably, the multi-path reflection is also included therein, and its effect elimination will be described in Section~\ref{Trace}.
Each body segment can be viewed as a virtual signal transmitter.
Hence, the baseband signal represents the signal transmitted from the virtual signal transmitter to the microphone array. 
The virtual signal senders (corresponding to different body segments) have different speeds during human walks or moves, thus generating baseband signals at different frequencies.  
As such, Eqn.~(\ref{1114}) can be considered as the superimposition of multiple baseband signals with different delays.
Our goal is to separate the 	$\sum S_{r}$ into multiple narrowband signals corresponding to  different body segments.


We next analyze the spectrogram of  $\sum S_{r}(t)$.
Considering the human walking/moving speed in a room is no more than $4m/s$, we only need to take into account signals at the frequency range of  $19.5Hz$ to $20.5Hz$, to sufficiently cover all Doppler shifts from a moving person. 
Assume $F(f, t)$ represents the Doppler energy of frequency $f$ at a certain time $t$ on the spectrogram.
Since $F(f, t)$ changes with the frequency, by analyzing its peaks, 
we can identify the frequency components of  $\sum S_{r}(t)$  that signify the Doppler frequencies of different body segments.
Our empirical study exhibits  the averaged difference between two consecutive frequency components is about $20Hz$.
Thus, we divide the entire spectrogram into a set of $20Hz$ narrowband signals.
The $20Hz$ band results in $0.05m/s$ velocity resolution, sufficiently to differentiate the Doppler shifts from different body segments.
As such, each narrowband signal  can be  represented as: 
\begin{equation}
	\begin{aligned}
		S_{r}(t)=\alpha_{m} \cos \left(2 \pi f_{m} (t-\tau_{m})\right), \label{1115}
	\end{aligned}
\end{equation}
Notably,  $\tau_{m}$ is a one-way TOF that  corresponds to the time taken by the signal to travel from a virtual signal sender to the microphone.
Since only the  frequency range of  $19.5Hz$ to $20.5Hz$ is taken into account with the narrowband signal of $20Hz$ bandwidth, the microphone covers 48 narrowband signals.
But, not all narrowband signals contain the Doppler reflections from moving body segments, so those narrowband signals whose  averaged amplitudes below a predefined threshold are discarded.

\subsection{Signal Modeling}
Each   narrowband signal is considered to be associated with one specific virtual signal transmitter. 
We then perform I-Q decomposition to analyze its phase, which is affected by $\tau_{m}$ and determined by the range, angle, and velocity, according to Eqn.~(\ref{1115}).
After the phase of the narrowband signal is analyzed, we then  build a signal model to 
formulate the narrowband signal in terms of three parameters, {\em i.e.,} range, angle, and velocity. 


\begin{figure}[htb]
	\centering
	\includegraphics[width=4.0in]{Fig/IQ.pdf}
	\caption{I-Q decomposition.}
	\label{IQdecomposition}
\end{figure}


\noindent\textbf{I-Q decomposition:} Figure~\ref{IQdecomposition} illustrates the process of  I-Q decomposition. 
The narrowband signal $S_{r}(t)$ is multiplied by the continuous wave of $cos 2\pi f_{m}(t)$ and 
its 90-degree phase-shifted version of $sin 2\pi f_{m}(t)$, where $f_{m}$ denotes  the frequency when the original $20kHz$ signal is received by the moving  body segment, and it can be calculated via Eqn.~(\ref{doppler_formula1}). 
Since $v$ is unknown yet, so we need to calculate $v$ first, done by directly applying FFT to the narrowband signal to get its frequency $f_{v}$.
According to Eqn.~(\ref{doppler_formula}), we can derive the velocity $v$ of the narrowband signal and then apply Eqn.~(\ref{doppler_formula1}) to obtain  $f_{m}$.



\begin{figure*}
	\centering
	\begin{minipage}{0.41\textwidth}
		\centering
		\includegraphics[width=2.5in]{Fig/Isignal.pdf}
		\caption{I-signal waveform.}
		\label{Isignal}
	\end{minipage}
	\hspace{1em}
	\begin{minipage}{0.41\textwidth}
		\centering
		\includegraphics[width=2.5in]{Fig/Qsignal.pdf}
		\caption{Q-signal waveform.}
		\label{Qsignal}
	\end{minipage}
\end{figure*}



With $f_{m}$, we next perform  I-Q decomposition. 
According to the fact of  $\cos A  \cos B=\frac{1}{2}(\cos (A+B)+\cos(A-B))$, we  get  $\cos(A-B)$ by  filtering out the high frequency component of $\cos (A+B)$ through a low pass filter.
Thus,  In-Phase signal $I(t)$ and   Quadrature   signal $Q(t)$  can  be represented as:
\begin{equation}
	\begin{aligned}
		&I(t) = \frac{1}{2} \alpha_{1} \cos 2 \pi  f_{m} \tau,  \ 
		Q(t) = \frac{1}{2} \alpha_{1} \sin 2 \pi  f_{m} \tau. \label{IandQ}  \\  
	\end{aligned}
\end{equation}
Figure \ref{Isignal} and  Figure~\ref{Qsignal} present the 0.1s examples of  In-Phase signal and of Quadrature   signal, respectively, with the baseband frequency equal to $20.045kHz$. 
From the two figures, we  clearly observe a phase change regarding the waveforms of  I-signal and Q-signal.
By combing them, we arrive at a complex signal, denoted by
\begin{equation}
	S^{M}(t)=I(t)+j Q(t)=\frac{1}{2} \alpha_{1} e^{j 2 \pi f_{m} \tau  }.
	\label{combineIQ}
\end{equation}
Figure \ref{IQtrace} shows the I-Q trace of a complex signal, in which the  I-Q trace moving around one circle corresponds to a $2 \pi$ phase change.
From Eqn.~(\ref{combineIQ}), the phase change  is caused by the change of $\tau$.
\begin{figure}[htb]
	\centering
	\includegraphics[width=3.0in]{fig/IQtrace.pdf}
	\caption{Complex I-Q trace.}
	\label{IQtrace}
\end{figure}






\noindent\textbf{Phase Analysis:} We next analyze the phase of a complex signal by first obtaining  $\tau$, since it determines the phase.
Notably, $\tau$ is the one-way traveling time from an associated moving body segment to the receiver, and it varies according to the change of distance between the segment and the  receiver.
Suppose that a target is moving across the distance of $r(t)$ and  with the angle of $\theta$.
Then,  $r(t)$   causes a one-way time of $\frac{r(t)}{c}$, where $c$ is the sound speed.
\begin{figure}
	\centering
	\includegraphics[width=3.2in]{fig/array.pdf}
	\caption{Additional distance caused by the microphone array structure.}
	\label{array}
\end{figure}
Our experiment employs a liner 4-microphone array, with the distance between its two adjacent microphones   equal to $d$. 
As Figure~\ref{array} shows, for the $k^{\text {th }}$ microphone, the respective ToF of a  received signal travels for an extra time of $\frac{(k-1)d \cos \theta}{c}$ compared to that received by the first microphone (the rightmost one).
Hence, ToF for the signal received at the $k^{\text {th}}$ microphone  can be computed by
\begin{equation}
	\tau=\frac{r(t)}{c}+\frac{(k-1)d \cos \theta}{c}. \label{tof}
\end{equation}
With Eqn.~(\ref{combineIQ}), we have the complex signal at the $k^{\text {th}}$ microphone:
\begin{equation}
	S^{M}(t)=I(t)+j Q(t)=\frac{1}{2} \alpha_{1} e^{j 2 \pi f_{m} (\frac{r(t)}{c}+\frac{(k-1)d \cos \theta}{c})   },	
\end{equation}
where  $f_{m}$ is the baseband frequency that already has  been estimated.
In Figure~\ref{IQtrace}, the I-Q trace combines 0.1s worth of I-single and Q-signal, and the baseband frequency $f_{m}$ is $20.045kHz$.
There are about 4.3 circles in the figure, and the phase change of $\tau$ in 0.1s is $4.3 \times 2 \pi = 8.6\pi$.
In this small time period, the velocity and AoA are deemed constant. 
So, the distance change is due solely to a change in $r(t)$.
We can calculate the distance change $\Delta r$  through the accumulated phase change of $\tau$ given by $ 2 \pi \frac{f_{m} \Delta r}{c}=8.6 \pi$  to yield $\Delta r=0.073m$.

To mathematically model this, we consider the velocity $v$ of each moving body segment to be constant.
Then,  $r(t)$ can be denoted as $r(t)=r+vt$, where $r$ is the initial range. 
 $S^{M}(t)$ is thus expressed by:
\begin{equation}
	S^{M}(t)=I(t)+j Q(t)=\frac{1}{2} \alpha_{1} e^{j 2 \pi f_{m} (\frac{r}{c}+\frac{vt}{c}+\frac{(k-1)d \cos \theta}{c})   }. \label{combine}
\end{equation}
We represent this signal model as $S^{M}= \frac{1}{2} \alpha  \cdot R  \cdot V \cdot \Theta_{k} $, where  $R  , V , $ and $\Theta_{k} $ are defined as follows:
\begin{equation}
	\begin{aligned}
		%&F=e^{j 2 \pi  f_{small}t} \\
		&R(r)=e^{j 2 \pi \frac{r}{c}f_{m}}, \\
		&V(v)=e^{j 2 \pi \frac{vt}{c}f_{m}},\\
		&\Theta_{k}(\theta)=e^{j 2 \pi \frac{(k-1)d \cos \theta}{c}f_{m}}\label{vector}. \\ 
	\end{aligned}
\end{equation}
Since $v$ is already calculated by the previous step, we only need to consider $R$  and $\Theta $ components when analyzing  $r$ and $\theta$ of each body segment in its associated narrowband signal.





\subsection{Target Estimating}
\label{Multi Target Estimator}
In this section, we first  estimate   $r$ and then $\theta$ of each narrowband signal and cluster the estimated parameters  corresponding to the same person.

\subsubsection{Motion State Estimator}
In each round of estimation, we take into account those narrowband signals that last for $0.1s$.
Given the sampling rate of $44.1kHz$, each narrowband signal contains $N=4410$ samples.
As discussed in Section~\ref{slicesignla}, the narrowband signals that  contain no Doppler reflections are discarded.
We denote the number of  remaining narrow bands as $B$ and represent all narrowband signals by  $\Sigma=\left[S_{r1}, S_{r2} , S_{r3} ,\cdots ,S_{rb},\cdots , S_{rB}\right]^T$. 
On each narrowband, since we have signals from M microphones, after I-Q decomposition,  $S_{rb}$ can be represented as $S_{rb}=\left[S^{b1}, S^{b2} , \cdots, S^{bk}, \cdots, S^{bM}\right]^T$, where $S^{bk}$ denotes the signals received at the $k$th microphone. 
Since $S^{b1}, S^{b2} , \cdots, S^{bM}$ are considered to have the same $r, \theta, $ and $v$, according to Eqn.~(\ref{vector}), we can formulate an optimization problem for each narrowband signal $S_{rb}$ as follows. 
\begin{equation}
	(\theta, r)= {\arg \max } |\sum_{k=1}^{M} S^{bk} \cdot R^{*} (r)  \cdot \Theta_{k}^{*}(\theta)|, \label{optimize}
\end{equation}
where $(\cdot)^{*}$ indicates the conjugate operation. 
With the correct $\theta$ and $ r$ are estimated, Eqn.~(\ref{optimize}) gives rise to the local maximum value.
Due to the searching range of ($-90^{\circ},90^{\circ}$) for $\theta$, from Eqn.~(\ref{vector}), we can see $\Theta^{*}(\theta)$ is not a periodic function. 
Hence,  $\theta$ has a unique solution corresponding to each $S_{ri}$.
However, since  $r$ is in the range from $0m$ to several meters, $R^{*} (r)$ is a periodic function, making the the optimal solution of $r$ nonunique and calling for a further process to estimate the correct $r$.


Consider the two body segments of upper limbs with their motion states $(r_{1},v_{1},\theta_{1})$ and  $(r_{2},v_{2},\theta_{2})$, respectively. Although their speeds and angles may be different due to the motions of body segments,   their horizontal ranges are similar. 
As such, to get the correct $r$, 
we can consider that some  body segments from the same person  have the same distance to the microphone array.
Eqn.~(\ref{combine}) reveals that each narrowband signal's initial phase is affected by   $r$ and $\theta$.
Since we can remove  $\Theta$ from a narrowband signal by multiplying its corresponding $\Theta_{k}^{*}$, the initial phase of the remaining signal is  determined solely by $r$.
Denote  $\phi_1$ and  $\phi_2$, as the initial phases of two narrowband signals, which can be extracted directly from signals themselves. 
Assuming their corresponding frequencies are $f_{m1}$ and $f_{m2}$ after removing  $\Theta$. 
According to Eqn.~(\ref{combine}), for two narrowband signals with the same distance, we have $\phi_1=2 \pi f_{m1}\frac{r}{c}$ and $\phi_2=2 \pi f_{m2}\frac{r}{c}$, which give rise to  $\frac{\phi_1}{f_{m1}}=\frac{\phi_2}{f_{m2}}$, implying that the ratio of the initial phase over the baseband frequency is identical for  all signals with the same distance. 
Based on this result, we calculate phase-frequency ratios with respect to all $Z$  narrowband signals. 
For each narrowband signal, we identify another one with the closest phase-frequency ratio for conjecture to have the similar distance. 
This results in a total of $Z$ pairs, with each pair considered to have the same distance. 
For each pair of narrowband signals with their frequencies of ($f_{m1}$, $f_{m2}$) and their initial phases of $(\phi_1, \phi_2)$, we have $2 \pi f_{m1}\frac{r}{c}-2 \pi f_{m2}\frac{r}{c}=\phi_1-\phi_2$.
Then, the distance $r$ can be calculated by  $r=\frac{\Delta \phi c}{\Delta f}$, where  $\Delta \phi $ is the initial phase difference and $\Delta f$ is the baseband frequency difference of  two baseband signals in this pair.
Next, $r$ is fed to the optimizer characterized by Eqn.~(\ref{optimize}) for refinement. 
That is, we slightly shift $r$'s value to find the closest optimal solution. 
In the end, we can have $Z$ sets of ($r,v,\theta$) values in total.


Until now, we get the motion states from different body segments and are yet to identify the moving person. 
The next subsection describes a clustering algorithm to associate all proper  ($r,v,\theta$)  values with  the same person, given that those ($r,v,\theta$) values represent the person's motion states.








\subsubsection{Clustering   Motion States}

The reflections from different body segments of a moving person have relatively identical $\theta $ and $ r$ values, even their $v$ values are different. 
On the other hand, the reflections from body segments of different persons possess markedly different $(\theta, r, v)$ values. 
Based on these facts, it is possible to cluster $(\theta, r, v)$ values associated with the same person together. 
Here, we apply the $K$-means algorithm to perform such clustering over $Z$ sets of ($r,v,\theta$) values, with each set treated as one point in the 3-D space and inputted to the algorithm.



We employ the silhouette value \cite{thinsungnoena2015clustering}  to help improve our $K$-means clustering.
That is, corresponding to each point $i$,  the silhouette value can be defined as follows: 
$$
s_{i}=\frac{b_{i}-a_{i}}{\max \left\{a_{i}, b_{i}\right\}},
$$
where $a_{i}$ indicates the averaged distance from the $i$th point to all other points in the same cluster, and $b_{i}$ is the minimum averaged distance from the $i$th point to points in each other cluster. 
So, the silhouette value ranges from -1 to +1, with a high value signifying the object  well matched with its own cluster and poorly matched to other clusters.
If the majority of points have the high silhouette values,  good clustering  results. 
Otherwise,  the clustering configuration is inappropriate, likely to have too many or too few clusters.
Here, the averaged silhouette value over all data points quantifies the clustering effectiveness degree. 
We explore $K$ in a proper range to select the  value which yields the highest averaged silhouette value.


After getting a clustering configuration, potential errors may still exist.
For example, one person's corresponding points may be separated into multiple clusters. 
In this case, we rely on the inter-cluster distance to further mitigate errors, by calculating the euclidean distance between two clusters' centroids. 
If the inter-cluster distance  of two clusters is below a pre-defined threshold, they are merged into one cluster. 



Ideally, we aim to cluster all points corresponding to the same person together through this step. 
However, the multipath effect may cause wrong $(r,v,\theta)$ values.
Besides, the same narrowband may include the signals of different body segments, albeit to a low probability but warranting further refinement.  The next subsection outlines a trace estimation solution for addressing aforementioned issues to enhance localization through refining our results. 


\subsubsection{Trace Identification} 
\label{Trace}
\begin{figure}[htb]
	\centering
	\includegraphics[width=3.0in]{fig/multipath3.pdf}
	\caption{Identifying the target points with the help of the previous estimation.}
	\label{multipath3}
\end{figure}
Given  that a person's movement is continuous, the two consecutively estimated ($r,v,\theta$) values of the same person should be similar. 
Since a rather short signal of 0.1s is considered at a time, the changes in a person's angle, range, and velocity are very small. 
Hence, we can take into account a sequence of two consecutive 0.1s estimations  to measure their difference per pair for the trace identification of a moving person. 
For each cluster $j$ at the time point of $t-1$, we calculate its centroid, i.e., the averaged $r,v,$ and $\theta$ values over all points, indicated as ${r}_{t-1}(j), {v}_{t-1}(j)$ and ${\theta}_{t-1}(j)$, respectively.
Similarly, for each cluster $i$ at the time point of $t$, we calculate the averaged $r,v,$ and $\theta$ values over all points, denoted as ${r}_{t}(i), {v}_{t}(i)$ and ${\theta}_{t}(i)$, respectively.
For each cluster pair of $i$ and $j$, their L-1 distance  is obtained by
\begin{equation}
	L1 (i,j) = \left|{r}_{t}(i)-{r}_{t-1}(j)\right|+\left|{v}_{t}(i)-{v}_{t-1}(j)\right|+\left|{\theta}_{t}(i)-{\theta}_{t-1}(j)\right|.
\end{equation}
For each $i$, we  find corresponding $j$ that has the  minimum $L1 (i,j)$.
Notably, if $i$ and $j$ are associated with the same person, the L-1 distance should be very small. 
Otherwise, if $i$ is from a multi-path reflection, its corresponding  L-1 distance tends to be large due to the quick change of reflection paths. 
This way identifies the moving trace of a person, starting from a cluster associated with the person.
Figure~\ref{multipath3}  show an example for cluster centroids  of two consecutive 0.1s signals,  plotted in a 2-D plane. 
In the figure, the green points show the currently estimated points, and red points show the previously estimated points,  plotted in a 2-D plane.
Comparing the previous points and current points, we can see the positions of the circled points have almost no changes, whereas the positions of other points change largely. 
Thus the circled points are considered as the target points from the same person. 
Other points are considered to be caused by multi-path reflections.





