\section{RELATED WORK}
\label{sec:related}


Acoustic signal applications have gained increasing interest in the research community. 
Among them,   acoustic-based tracking has  become prevalent in recent years, by taking advantage of widely available speakers and microphones built in commodity devices. 
This section discusses the current state-of-the-arts in acoustic localization and differentiates them from our work.


\subsection{Acoustic-based Tracking}
Some research efforts~\cite{yun2015turning, aumi2013doplink, huang2014shake, wang2014ubiquitous, yun2015turning, wang2019millisonic, mao2016cat, zhang2017soundtrak} entail to develop the device-based tracking by requiring a user to carry the device for localization.
This line of solutions indirectly tracks human motions by sensing the movement of carried devices.
But, they are inconvenient and impractical in many scenarios, since a person may forget or be reluctant to carry the device.
In contrast, our system belongs to device-free tracking, freeing a user from carrying any device to make it more attractive. 


Previous device-free tracking  mainly relies on controlling the speaker to transmit chirp signals (i.e., OFDM, FMCW, Zadoff-Chu (ZC) sequence) and analyzing  reflected signals for tracking. 
For example, \cite{nandakumar2016fingerio, nandakumar2017covertband} have exploited the correlation of OFDM signals for measuring the time-of-flight (ToF) for finger and activity tracking. 
\cite{li2020fm,lian2021echospot, zhou2017batmapper} leveraged the FMCW signal and developed a  series of signal processing techniques for motion tracking in the room-scale environment and indoor floor plan mapping. 
\cite{murano2018comparison} leveraged the ZC sequence to infer  user's hand patterns when tapping the  PIN code.
In contrast, our system achieves target localization in approximately 0.63 seconds.
	Also they are using FMCW signals, and the chirp signal-based solutions typically require large bandwidth for accurate tracking (i.e., $16khz$ to $20khz$).
	Although they work on the inaudible ultrasound range, some  people may still perceive such wideband ultrasound disturbingly, as a result of frequency changes.
	Another potential drawback is that the audible ``Beep'' sound may be produced, annoying human life.
	Differently, our system relies on emitting the continuous wave for sensing, which can substantially avoid such drawbacks.
	



Some earlier methods \cite{mao2019rnn, mao2020deeprange} have demonstrated the viability of neural networks in aiding localization. 
For example, \cite{mao2019rnn} utilized both the 2D MUSIC algorithm and beamforming to extend the sensing range, coupled with a recurrent neural network (RNN) to determine target locations. 
\cite{mao2020deeprange} directly inputted data into a deep neural network (DNN) for finger tracking.
One noteworthy distinction in our approach is its ability to operate directly without collecting additional data and labeling processes while achieving room-scale tracking.
In addition, our approach takes into account practical considerations, such as the limited hardware and computational constraints of Commercial off-the-shelf (COTS) devices, which might raise challenges for the applicability of existing methods.
Although taking longer than earlier methods (i.e., 0.63 sec versus some 0.05 sec in \cite{mao2019rnn, mao2020deeprange}) for localization, our approach nonetheless is still considered to exhibit near real-time localization.


Our research closely related to \cite{wang2016device}, where  demonstrated the feasibility of tracking the hand movement for the first time by utilizing the phase information of  continuous waves.
That work analyzed the signal phase by acquiring the difference between the baseband signal and the reflected signal via  I-Q decomposition \cite{wang2016device}.
However, it considered the frequency of  baseband signals to be the same as that of  original signals, potentially resulting in excessive interference between them.
Since the acoustic signal experiences fast attenuation with  the distance, the  reflection signals over a long range become rather weak and thus are hard to be separated from the strong baseband signal, rendering it suitable only for limited-range sensing.
In contrast, our work leverages the phase of   Doppler signals for analysis, which has a large difference from that of the baseband signal to yield clean phase information after I-Q decomposition.
The tracking range can then be substantially extended, applicable to  room-scale environments. 
Note that the phase of chirp signals was analyzed in certain prior studies for motion tracking. 
For example, \cite{yun2017strata} relied on the phase change of OFDM signals to track fine-grained chest movements for respiration detection.
\cite{zhang2018vernier} leveraged the phase of FMCW signals to infer the finger position, whereas
\cite{sun2018vskin} tracked the finger movements by combining the ZC sequence phase and amplitude information.
However, it's worth noting that these systems are primarily designed for finger tracking and may not be suitable for room-scale localization.
Meanwhile, they underperform our approach, which directly measures the phase of continuous waves over a set of narrowband signals that incur low noise for sound phase estimation.









\subsection{RF-based Localization}
Next, we briefly review RF-based solutions for sensing and tracking target movement, contrasting them with our approach in the acoustic domain.
RF-based solutions can be summarized into three categories:
Radar-based sensing, RFID-based sensing, and Wi-Fi based sensing.

Many efforts on radar-based sensing \cite{adib2015multi, joshi2015wideo, zhu2015reusing, wei2015mtrack, leong2013multiple, zhu2015reusing, wei2015mtrack, gu2019mmsense, zhao2019mid} have been conducted for single/multi-target tracking.
They all call for large bandwidth and the antenna array(s) to acquire signals, requiring  specialized hardware to transmit and receive signals to incur considerable extra costs.
RFID-based solutions~\cite{wang2007rfid, maneesilp2012rfid, wang2013rf, wang2013dude, ni2003landmarc, jin2006indoor} for human motion tracking were also considered, by leveraging the RFID tags and readers for localization. 
However, they  typically require  users to wear the RFID tags for localization, deemed inconvenient and cumbersome.
Although some device-free tracking solutions~\cite{yang2020rf, chen2020tagray} have been proposed, they experience   limited sensing ranges, usually in tens of centimeters to make them unsuitable for the room-scale environment.
Location trace tracking techniques based on the Wi-Fi signals have been widely studied \cite{adib2014witrack, adib2015multi, joshi2015wideo, sun2015widraw, wang2018low, xie2019md, ayyalasomayajula2020deep}.
However, they utilize large bandwidth to achieve satisfactory localization accuracy, occupying the communication channels at 2.4GHz or 5GHz  and thus negatively impacting the operations of nearby Wi-Fi-based devices to a certain extent.
In addition, most of them involve multiple transmission links, often calling for large antenna arrays and customized hardware for processing to hinder their adoption for commodity device-based applications. 












 




